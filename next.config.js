const path = require('path');
const webpack = require('webpack');

module.exports = {
    webpack(config) {
        config.resolve.alias.components = path.join(__dirname, 'src/components');
        config.resolve.alias.pages = path.join(__dirname, 'src/pages');
        config.resolve.alias.constants = path.join(__dirname, 'src/constants');
        config.resolve.alias.utils = path.join(__dirname, 'src/utils');
        config.resolve.alias.store = path.join(__dirname, 'src/store');
        config.resolve.alias.reducers = path.join(__dirname, 'src/reducers');
        config.resolve.alias.actions = path.join(__dirname, 'src/actions');
        config.resolve.alias.hocs = path.join(__dirname, 'src/hocs');

        return config;
    }
};
