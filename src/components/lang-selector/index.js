import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { withTranslation } from 'react-i18next';

import styles from './styles.module.css';

const options = [
    { id: 'ru', title: 'Русский', imgUrl: 'images/ru-icon.png' },
    { id: 'kz', title: 'Қазақ', imgUrl: 'images/kz-icon.png' },
    { id: 'uz', title: 'O‘zbek', imgUrl: 'images/uz-icon.png' }
];

class Selector extends React.Component {
  constructor() {
    super();

    this.state = {
      anchorEl: null,
      selectedIndex: 1
    };
  }

  componentDidMount() {
    const index = this.getLangIndexById(localStorage.language);

    this.setState({
      selectedIndex: index
    });
    this.props.i18n.changeLanguage(options[index].id);
  }

  setAnchorEl = (el) => {
    this.setState({
      anchorEl: el
    });
  };

  setSelectedIndex = (index) => {
    this.setState({
      selectedIndex: index
    });
  };

  getLangIndexById = (id) => options.findIndex((item) => item.id === id);

  handleClickListItem = (event) => {
    this.setAnchorEl(event.currentTarget);
  };

  handleMenuItemClick = (event, index) => {
    this.setSelectedIndex(index);
    this.setAnchorEl(null);

    this.props.i18n.changeLanguage(options[index].id);
    localStorage.language = options[index].id;
  };

  handleClose = () => {
    this.setAnchorEl(null);
  };

  render() {
    return (
        <div>
            <List component="nav" aria-label="Device settings">
                <ListItem
                    button
                    aria-haspopup="true"
                    aria-controls="lock-menu"
                    aria-label="язык"
                    onClick={this.handleClickListItem}
                >
                    <ListItemText primary={this.props.t('language')} secondary={options[this.state.selectedIndex].title} />
                    <img src={options[this.state.selectedIndex].imgUrl} className={`${styles.icon} pl-2`} />
                </ListItem>
            </List>
            <Menu
                id="lock-menu"
                anchorEl={this.state.anchorEl}
                keepMounted
                open={Boolean(this.state.anchorEl)}
                onClose={this.handleClose}
            >
                {options.map((option, index) => (
                    <MenuItem
                        key={option.id}
                        selected={index === this.state.selectedIndex}
                        onClick={(event) => this.handleMenuItemClick(event, index)}
                    >
                        <div className="d-flex align-items-center justify-content-between w-100">
                            <p className="pr-2 mb-0">{option.title}</p>
                            <img src={option.imgUrl} className={`${styles.icon}`} />
                        </div>
                    </MenuItem>
        ))}
            </Menu>
        </div>
  );
  }
}

export default withTranslation()(Selector);
