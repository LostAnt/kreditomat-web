import styles from './styles.module.css';

export default (props) => (
    props.show ? (
        <div className={`${styles.container}`}>
            <div className="spinner-border" style={{ width: '3rem', height: '3rem' }} role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    ) : null
);
