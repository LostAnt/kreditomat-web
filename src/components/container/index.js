
const layoutStyle = {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '75%',
    height: '100%',
    minHeight: '100%'
};

const Layout = (props) => (
    <div style={layoutStyle}>
        {props.children}
    </div>
);

export default Layout;
