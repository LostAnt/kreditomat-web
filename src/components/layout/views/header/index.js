import Container from 'components/container';
import Link from 'next/link';
import { useTranslation } from 'react-i18next';

import LangSelector from 'components/lang-selector';
import CitySelector from 'components/city-selector';

import { getPhone } from 'utils/contacts';

import styles from './styles.module.css';

const headerStyle = {
    width: '100%',
    height: '120px'
};

const Header = () => {
    const { t } = useTranslation();

    const [phone, setPhone] = React.useState('');

    React.useEffect(() => {
        setPhone(getPhone());
    }, []);

    return (
        <Container>
            <div className={`d-flex align-items-center justify-content-between w-100 ${styles.container}`} style={headerStyle}>
                <div className="d-flex">
                    <CitySelector />
                    <LangSelector />
                </div>
                <div className={`d-flex flex-column align-items-end justify-content-center ${styles.container}`}>
                    <div className="d-flex align-items-center justify-content-center">
                        <div className={`${styles.contacts}`}>
                            <div className="d-flex">
                                <p className={`mb-0 mr-3 ${styles.number} text-center`}>{phone}</p>
                                <p className={`mb-0 mr-3 ${styles.email}`}>info@kreditomat24.ru</p>
                            </div>
                            <p className={`mb-0 mr-3 ${styles['number-hint']} text-center`}>{t('header.worktime')}</p>
                        </div>
                        <a href="https://www.vk.com/kreditomat24" target="_blank" rel="noopener noreferrer">
                            <img src="images/vk_icon.png" className={`${styles['icon-image']} mr-3`} />
                        </a>
                        <a href="https://www.instagram.com/kreditomat24" target="_blank" rel="noopener noreferrer">
                            <img src="images/inst_icon.png" className={`${styles['icon-image']} mr-3`} />
                        </a>
                        <img src="images/viber_icon.png" className={`${styles['icon-image']} mr-3`} />
                        <img src="images/ws_icon.png" className={`${styles['icon-image']} mr-2`} />
                    </div>
                    <div className="mt-2">
                        <Link href="profile">
                            <button type="button" className={`${styles['lk-button']}`}>{t('header.lk')}</button>
                        </Link>
                    </div>
                </div>
            </div>
        </Container>
);
    };

export default Header;
