import { useTranslation } from 'react-i18next';

import Link from 'next/link';
import Container from 'components/container';

import NavbarItem from './views/navbar-item';

import styles from './styles.module.css';

const navbarStyle = {
    width: '100%',
    height: '50px',
    marginBottom: '20px'
};

const Navbar = () => {
    const { t } = useTranslation();

    return (
        <Container>
            <div className="d-flex align-items-end justify-content-start" style={navbarStyle}>
                <Link href="home">
                    <img src="images/main_logo.png" className={`${styles['logo-image']} mr-5`} />
                </Link>
                <NavbarItem title={t('navbar.about')} page="about-company" />
                <NavbarItem title={t('navbar.terminals')} page="terminals" />
                <NavbarItem title={t('navbar.howget')} page="get-loan-instruction" />
                <NavbarItem title={t('navbar.howpay')} page="pay-loan-instruction" />
                <NavbarItem title={t('navbar.faq')} page="faq" />
            </div>
        </Container>
    );
};

export default Navbar;
