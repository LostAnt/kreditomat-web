import Link from 'next/link';

import styles from './styles.module.css';

export default ({ title, page }) => (
    <Link href={page}>
        <p className={`pb-1 pr-2 pl-2 mr-4 mb-0 text-center ${styles['navbar-item']}`}>{title}</p>
    </Link>
);
