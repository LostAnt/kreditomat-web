import { useTranslation } from 'react-i18next';

import Link from 'next/link';
import Container from 'components/container';

import styles from './styles.module.css';

const headerStyle = {
    width: '100%',
    height: '60px',
    'margin-bottom': '20px'
};

const Header = () => {
    const { t } = useTranslation();

    return (
        <Container>
            <div className="d-flex align-items-end mt-3" style={headerStyle}>
                <p className={`text-center ${styles.text}`}>© 2020, ООО МФК “Паскаль” (микрофинансовая компания), осуществляющая свою деятельность в режиме онлайн под контролем Центрального Банка Российской Федерации. Свидетельство о внесении сведений в государственный реестр микрофинансовых организаций №651403550005450 от 28.07.2018 года.</p>
            </div>
        </Container>
    );
};

export default Header;
