import Header from './views/header';
import Footer from './views/footer';
import Navbar from './views/navbar';

const layoutStyle = {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    width: '100%',
    minHeight: '100vh'
};

const contentStyle = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100%',
    width: '100%'
};

const Layout = (props) => (
    <div className="Layout" style={layoutStyle}>
        <Header />
        <Navbar />
        <div className="Content" style={contentStyle}>
            {props.children}
        </div>
        <Footer />
    </div>
);

export default Layout;
