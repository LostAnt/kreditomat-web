
const layoutStyle = {
  marginLeft: "auto",
  marginRight: "auto",
  width: "65%"
};

const contentStyle = {
  flex: 1,
  display: "flex",
  flexDirection: "column"
};

const Layout = props => (
    <div style={layoutStyle}>
        {props.children}
    </div>
);

export default Layout;