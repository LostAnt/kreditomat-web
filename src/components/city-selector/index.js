
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { withTranslation } from 'react-i18next';
import Router from 'next/router';

const options = [
  { id: 'vor', title: 'Воронеж' },
  { id: 'spb', title: 'Санкт-Петербург' }
];

class Selector extends React.Component {
  constructor() {
    super();

    this.state = {
      anchorEl: null,
      selectedIndex: 0
    };
  }

  componentDidMount() {
    const city = localStorage.city ? localStorage.city : options[0].id;
    const index = this.getLangIndexById(city);

    this.setState({
      selectedIndex: index
    });
  }

  setAnchorEl = (el) => {
    this.setState({
      anchorEl: el
    });
  };

  setSelectedIndex = (index) => {
    this.setState({
      selectedIndex: index
    });
  };

  getLangIndexById = (id) => options.findIndex((item) => item.id === id);

  handleClickListItem = (event) => {
    this.setAnchorEl(event.currentTarget);
  };

  handleMenuItemClick = (event, index) => {
    if (index === this.state.selectedIndex) {
      this.setAnchorEl(null);

      return;
    }

    this.setSelectedIndex(index);
    this.setAnchorEl(null);

    localStorage.city = options[index].id;
    localStorage.authToken = null;
    localStorage.refreshToken = null;

    Router.reload();
  };

  handleClose = () => {
    this.setAnchorEl(null);
  };

  render() {
    return (
        <div>
            <List component="nav" aria-label="Device settings">
                <ListItem
                    button
                    aria-haspopup="true"
                    aria-controls="lock-menu"
                    aria-label="язык"
                    onClick={this.handleClickListItem}
                >
                    <ListItemText primary={this.props.t('city')} secondary={options[this.state.selectedIndex].title} />
                </ListItem>
            </List>
            <Menu
                id="lock-menu"
                anchorEl={this.state.anchorEl}
                keepMounted
                open={Boolean(this.state.anchorEl)}
                onClose={this.handleClose}
            >
                {options.map((option, index) => (
                    <MenuItem
                        key={option.id}
                        selected={index === this.state.selectedIndex}
                        onClick={(event) => this.handleMenuItemClick(event, index)}
                    >
                        <div className="d-flex align-items-center justify-content-between w-100">
                            <p className="pr-2 mb-0">{option.title}</p>
                        </div>
                    </MenuItem>
        ))}
            </Menu>
        </div>
  );
  }
}

export default withTranslation()(Selector);
