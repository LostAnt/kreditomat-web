import { useTranslation } from 'react-i18next';

import Layout from 'components/layout';
import Container from 'components/reading-container';

import Map from './views/map';
import Contacts from './views/contacts';

import styles from './styles.module.css';

export default () => {
    const { t } = useTranslation();

    return (
        <Layout>
            <Container>
                <p className={`${styles['block-header']} w-100 text-center`}>{t('terminals.title')}</p>
                <div className="d-flex">
                    <div className="d-flex flex-column">
                        <p className={`${styles['step-title']}`}>{t('terminals.step1.title')}</p>
                        <p className={`w-100 ${styles.text}`}>
                            {t('terminals.step1.desc')}
                        </p>
                        <p className={`${styles['step-title']}`}>{t('terminals.step2.title')}</p>
                        <p className={`w-100 ${styles.text}`}>
                            {t('terminals.step2.desc')}
                        </p>
                    </div>
                    <div>
                        <img src="images/terminal.png" className={`${styles.image}`} />
                    </div>
                </div>
                <p className={`${styles['step-title']}`}>
                    {t('terminals.map.title')}
                </p>
            </Container>
            <div className="mb-3">
                <Map />
            </div>
            <Contacts />
        </Layout>
    );
};
