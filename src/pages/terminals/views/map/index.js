import {
    withScriptjs, withGoogleMap, GoogleMap, Marker
} from 'react-google-maps';
import { compose, withProps } from 'recompose';

import { getMapCenter, getMapPoints } from 'utils/terminals';

export default compose(
    withProps({
        googleMapURL: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBsO9dDK3dBryIYu2UpulzQ7VxBDFxAR10&v=3.exp&libraries=geometry,drawing,places',
        loadingElement: <div style={{ height: '100%' }} />,
        containerElement: <div style={{ height: '400px', width: '100%' }} />,
        mapElement: <div style={{ height: '100%' }} />,
    }),
    withScriptjs,
    withGoogleMap
)(() => {
    const [mapCenter, setMapCenter] = React.useState(getMapCenter());
    const [mapPoints, setMapPoints] = React.useState(getMapPoints());

    React.useEffect(() => {
        setMapCenter(getMapCenter());
        setMapPoints(getMapPoints());
    }, []);

    return (
        <GoogleMap
            defaultZoom={10.3}
            defaultCenter={{ lat: mapCenter.lat, lng: mapCenter.long }}
        >
            {mapPoints.map((item) => (
                <Marker position={{ lat: item.lat, lng: item.long }} />
            ))}
        </GoogleMap>
);
    });
