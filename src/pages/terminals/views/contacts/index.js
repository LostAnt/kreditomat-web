import { useTranslation } from 'react-i18next';

import Container from 'components/reading-container';

import { getAddresses } from 'utils/terminals';

import styles from './styles.module.css';

export default () => {
    const { t } = useTranslation();

    const [addresses, setAddresses] = React.useState([]);

    React.useEffect(() => {
        setAddresses(getAddresses());
    }, []);

    return (
        <Container>
            <div className="w-100 d-flex justify-content-center mb-4">
                <div className={`w-100 p-4 mt-4 position-relative ${styles['docs-container']}`}>
                    <span className={`${styles['contacts-title']}`}>{t('terminals.addressess.title')}</span>
                    {
                        addresses.map((item, index) => (
                            <p>
                                {index + 1}
                                .
                                {' '}
                                {item}
                            </p>
                        ))
                    }
                </div>
            </div>
        </Container>
    );
};
