import React from 'react';
import Router from 'next/router';
import Head from 'next/head';
import isEmpty from 'lodash/isEmpty';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchUser, resetUser } from 'actions/user';
import { fetchActiveLoan, resetActiveLoan } from 'actions/active-loan';
import { fetchCreditCards } from 'actions/credit-cards';
import { refreshTokens } from 'actions/auth';

import Layout from 'components/layout';
import Container from 'components/container';

import { api } from 'utils/apiClient';

import styles from './styles.module.css';

class Profile extends React.Component {
    anotherId = 'another';

    constructor(props) {
        super(props);

        this.state = {
            amount: 0,
            cardId: this.anotherId
        };
    }

    componentDidMount() {
        this.props.actions.refreshTokens(localStorage.authToken, localStorage.refreshToken).then(() => {
            this.props.actions.fetchActiveLoan();
            this.props.actions.fetchCreditCards();
            this.props.actions.fetchUser();
        }).catch(() => {
            Router.push('/auth');
        });

        // mandarinpay.hosted.setup('#form-hosted-pay', {
        //     operationId: '9874694yr87y73e7ey39ed80',
        //     onsuccess(data) {
        //       alert(`Success, id: ${ data.transaction.id}`);
        //     },
        //     onerror(data) {
        //       alert(`Error: ${ data.error}`);
        //     }
        //   });
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.activeLoan && this.props.activeLoan) {
            this.handleAmountChange(this.props.activeLoan.loanCostOnCurrentDate);
        }

        if (isEmpty(prevProps.profile) && !isEmpty(this.props.profile)) {
            const email = this.props.profile.person.personEmails.find((item) => item.email.isActive);
            const emailAddress = email ? email.email.emailAddress : '';

            if (!emailAddress.trim()) {
                Router.push('/profile');
            }
        }
    }

    componentWillUnmount() {
        this.props.actions.resetActiveLoan();
        this.props.actions.resetUser();
    }

    handleAmountChange = (value) => {
        this.setState({
            amount: value
        });
    }

    handleCardChange = (value) => {
        this.setState({
            cardId: value
        });
    }

    renderCards = () => {
        if (!this.props.creditCards) {
            return null;
        }

        const cards = this.props.creditCards.map((item) => {
            console.log(item);
            if (!item.cardNumber || !item.externalSystemId) {
                return null;
            }

            return (
                <div
                    key={item.id}
                    className={`border rounded-pill pl-4 mb-2 ${item.externalSystemId === this.state.cardId ? 'border-success' : ''}`}
                    onClick={() => this.handleCardChange(item.externalSystemId)}
                >
                    <p className="mb-0 pt-3">{item.cardHolderName}</p>
                    <p>{item.cardNumber}</p>
                </div>
            );
        });

        return cards;
    };

    handleAddCard = () => {
        api.post('mandarin/bindcard', { redirectUrl: window.location.href }).then((response) => {
            window.location.href = response.redirectUrl;
        }).catch((error) => {
            this.setState({ error: 'Ошибка сервера. Повторите попытку позже.' });
        });
    }

    handlePayWithBindedCard = () => {
        api.post('mandarin/pay', { amount: this.state.amount, cardId: this.state.cardId, creditId: this.props.activeLoan.creditId }).then((response) => {
            Router.push('/profile');
        }).catch((error) => {
            this.setState({ error: 'Ошибка сервера. Повторите попытку позже.' });
        });
    }

    handlePayAnotherCard = () => {
        api.post('mandarin/payanother', { amount: this.state.amount, creditId: this.props.activeLoan.creditId, redirectUrl: `${window.location.origin }/profile` }).then((response) => {
            window.location.href = response.redirectUrl;
        }).catch((error) => {
            this.setState({ error: 'Ошибка сервера. Повторите попытку позже.' });
        });
    }

    handlePay = () => {
        if (this.state.cardId === this.anotherId) {
            this.handlePayAnotherCard();
        } else {
            this.handlePayWithBindedCard();
        }
    }

    render() {
        if (!this.props.isLoaded) {
            return (
                <Layout>
                    <div className={`d-flex align-items-center justify-content-center ${styles['spinner-container']}`}>
                        <div className="spinner-border" style={{ width: '3rem', height: '3rem' }} role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    </div>
                </Layout>
            );
        }
console.log(this.props);
        return (
            <Layout>
                <Container>
                    <p className={`${styles['block-header']} w-100 text-center mb-4`}>Погашение заема</p>
                    <p className={`${styles['step-title']}`}>Введите сумму</p>
                    <div className="d-flex d-column">
                        <input
                            className={`${styles.input} mb-3 mr-1`}
                            type="number"
                            value={this.state.amount}
                            onChange={(event) => this.handleAmountChange(event.target.value)}
                        />
                        <p>Руб.</p>
                    </div>

                    <p className={`${styles['step-title']}`}>Выберите карту </p>
                    <div className={`${styles['cards-container']} d-flex flex-column mb-5`}>

                        {
                        !this.props.creditCards || this.props.creditCards.length === 0 ? (
                            <div>
                                <p>Привязанных карт нет</p>
                            </div>
                        ) : (
                            <>
                                {this.renderCards()}

                            </>
                        )
                    }
                        <div
                            className={`border rounded-pill pl-4 mb-2 ${this.anotherId === this.state.cardId ? 'border-success' : ''}`}
                            onClick={() => this.handleCardChange(this.anotherId)}
                        >
                            <p className="pt-3">Использовать другую карту</p>
                        </div>
                        <button
                            type="button"
                            className="btn rounded-pill mb-md-1 button-brand-color mt-3"
                            onClick={this.handleAddCard}
                        >
                            Привязать новую карту
                        </button>
                        <button
                            type="button"
                            className="btn rounded-pill mb-md-1 button-brand-color mt-3"
                            onClick={this.handlePay}
                        >
                            Оплатить
                        </button>
                    </div>
                </Container>
            </Layout>
        );
    }
}

const mapStateToProps = (state) => ({
    profile: state.user.profile,
    isLoading: state.activeLoan.isLoading || state.user.isLoading,
    isLoaded: state.activeLoan.isLoaded,
    activeLoan: state.activeLoan.loan,
    loansHistory: state.loansHistory.loans,
    creditCards: state.creditCards.list
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({
        fetchUser,
        resetUser,
        fetchActiveLoan,
        resetActiveLoan,
        refreshTokens,
        fetchCreditCards
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
