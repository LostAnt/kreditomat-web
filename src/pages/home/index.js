import { useTranslation } from 'react-i18next';
import Carousel from 'react-multi-carousel';
import Modal from 'react-modal';
import Router from 'next/router';

import Layout from 'components/layout';
import Container from 'components/container';

import Replies from 'constants/replies';
import Calculator from './views/calculator';
import StepItem from './views/step-item';
import GetMethodItem from './views/get-method-item';


import styles from './styles.module.css';

const responsive = {
    superLargeDesktop: {
      breakpoint: { max: 4000, min: 3000 },
      items: 5
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1
    }
  };

const replies = Replies();

export default (props) => {
    const { t } = useTranslation();

    const [modalIsOpen, setIsOpen] = React.useState(false);

    const openModal = () => {
        setIsOpen(true);
    };

    const closeModal = () => {
        setIsOpen(false);
    };

    React.useEffect(() => {
        if (!localStorage.city) {
            openModal();
        }
    }, []);

    const onCitySelect = (city) => {
        localStorage.city = city;

        Router.reload();
    };

    const renderReply = (name, reply) => (
        <div className="d-flex flex-column">
            <div className={styles.replyContainer}>
                <p className={styles.replyText}>{reply}</p>
            </div>
            <p className={styles.replyName}>{name}</p>
        </div>
    );

    const ButtonGroup = ({
                            next, previous, goToSlide, ...rest
                        }) => {
        const { carouselState: { currentSlide } } = rest;
        return (
            <div className={styles.carouselButtons}>
                <img src="images/arrow_left.png" onClick={() => previous()} />
                <img src="images/arrow_right.png" onClick={() => next()} />
            </div>
        );
      };

    return (
        <Layout>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                contentLabel="Example Modal"
                style={{
                    overlay: {
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center'
                    },
                    content: {
                        position: 'relative',
                        left: '0',
                        top: '0',
                        bottom: '0',
                        right: '0',
                        height: '250px',
                        width: '500px',
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center'
                    }
                }}
            >
                <h5 className="pt-2 mb-5">{t('home.langselect.title')}</h5>
                <p className={styles.city} onClick={() => onCitySelect('vor')}>Воронеж</p>
                <p className={styles.city} onClick={() => onCitySelect('spb')}>Санкт-Петербург</p>
            </Modal>
            <Container>
                <div className={`${styles['main-image-container']}`}>
                    <img src="images/main_page_image.png" className={`${styles['main-image']}`} />
                    <div className={`${styles['promo-text']}`}>
                        <span className={`${styles['promo-text-line1']}`}>{t('home.promo.1')}</span>
                        <span className={`${styles['promo-text-line2']}`}>{t('home.promo.2')}</span>
                        <span className={`${styles['promo-text-line3']}`}>{t('home.promo.3')}</span>
                    </div>
                    <div className={`${styles['calculator-container']}`}>
                        <Calculator />
                    </div>
                </div>
            </Container>
            <div className={`${styles['block-purple']}`}>
                <Container>
                    <div className="d-flex align-items-center flex-column p-4">
                        <span className={`${styles['block-header']} text-white`}>{t('home.steps.title')}</span>
                        <div className="d-flex ">
                            <StepItem
                                imgUrl="images/step_person.png"
                                title={t('home.step1.title')}
                                description={t('home.step1.desc')}
                            />
                            <StepItem
                                imgUrl="images/step_request.png"
                                title={t('home.step2.title')}
                                description={t('home.step2.desc')}
                            />
                            <StepItem
                                imgUrl="images/step_card.png"
                                title={t('home.step3.title')}
                                description={t('home.step3.desc')}
                            />
                            <StepItem
                                imgUrl="images/step_money.png"
                                title={t('home.step4.title')}
                                description={t('home.step4.desc')}
                            />
                        </div>
                    </div>
                </Container>
            </div>
            <Container>
                <div className="d-flex align-items-center flex-column p-4">
                    <span className={`${styles['block-header']}`}>{t('home.methods.title')}</span>
                    <span className={`${styles['block-subtitle']}`}>{t('home.methods.subtitle')}</span>
                    <div className="d-flex w-100 justify-content-center mb-4">
                        <div className={`${styles['get-method-block']} d-flex mr-3 pt-4 pb-4 pl-4`}>
                            <GetMethodItem
                                imgUrl="images/method_master.png"
                                value="master"
                            />
                            <GetMethodItem
                                imgUrl="images/method_visa.png"
                                value="visa"
                            />
                            <GetMethodItem
                                imgUrl="images/method_mir.png"
                                value="mir"
                            />
                            <span className={`${styles['get-method-title']}`}>{t('home.method.card')}</span>
                        </div>
                        <div className={`${styles['get-method-block']} d-flex pt-4 pb-4 pl-4`}>
                            <GetMethodItem
                                imgUrl="images/method_cash.png"
                                value="cash"
                            />
                            <GetMethodItem
                                imgUrl="images/method_bank.png"
                                value="bank"
                            />
                            <span className={`${styles['get-method-title']}`}>{t('home.method.cash')}</span>
                        </div>
                    </div>
                    <button className={`${styles['get-money-button']} mb-3`}>
                        <span className={`${styles['get-money-button-text']}`}>{t('getmoney')}</span>
                    </button>
                </div>
            </Container>
            <div className={`${styles['block-purple']} mb-4`}>
                <Container>
                    <div className={`${styles['block-about']} d-flex align-items-center justify-content-between`}>
                        <span className={`${styles['about-header']}`}>{t('home.about.title')}</span>
                        <div className="d-flex flex-column">
                            <span className={`${styles['about-text']}`}>{t('home.about.subtitle')}</span>
                            <div className="d-flex align-items-center ml-4">
                                <div className={`${styles['about-circle']}`} />
                                <span className={`${styles['about-subtext']}`}>{t('home.about.fact1')}</span>
                            </div>
                            <div className="d-flex align-items-center ml-4">
                                <div className={`${styles['about-circle']}`} />
                                <span className={`${styles['about-subtext']}`}>{t('home.about.fact2')}</span>
                            </div>
                            <div className="d-flex align-items-center ml-4">
                                <div className={`${styles['about-circle']}`} />
                                <span className={`${styles['about-subtext']}`}>{t('home.about.fact3')}</span>
                            </div>
                            <div className="d-flex align-items-center ml-4">
                                <div className={`${styles['about-circle']}`} />
                                <span className={`${styles['about-subtext']}`}>{t('home.about.fact4')}</span>
                            </div>
                        </div>
                    </div>
                </Container>
            </div>
            <div className="d-flex align-items-center flex-column p-4 position-relative">
                <span className={`${styles['block-header']} pb-4`}>{t('home.reviews.title')}</span>
                <Carousel
                    responsive={responsive}
                    ssr
                    infinite
                    renderButtonGroupOutside
                    customButtonGroup={<ButtonGroup />}
                    arrows={false}
                    containerClass={styles.carouselContainer}
                    deviceType="desktop"
                >
                    {replies.map((item) => renderReply(item.name, item.reply))}
                </Carousel>
            </div>
        </Layout>
    );
};
