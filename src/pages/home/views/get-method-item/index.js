
import styles from './styles.module.css';

export default ({imgUrl, value}) => {
    return (
        <label htmlFor={value} className={`d-flex align-items-center ${styles['container']} mr-4 justify-content-between`}>
            <input type="radio" id={value} name="getmethod" value={value} />
            <div className={`${styles['image-container']}`}>
                <img src={imgUrl} className={`${styles['step-image']}`}/>
            </div>
        </label>
    );
}