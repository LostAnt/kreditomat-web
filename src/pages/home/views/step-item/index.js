
import styles from './styles.module.css';

export default ({imgUrl, title, description}) => {
    return (
        <div className={`d-flex flex-column align-items-center ${styles['container']}`}>
            <div className={`${styles['image-container']}`}>
                <img src={imgUrl} className={`${styles['step-image']}`}/>
            </div>
            <span className={`${styles['title']}`}>{title}</span>
            <span className={`${styles['description']}`}>{description}</span>
        </div>
    );
}