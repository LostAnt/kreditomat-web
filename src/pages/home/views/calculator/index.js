import { useState } from 'react';
import Moment from 'moment';
import { useTranslation } from 'react-i18next';

import styles from './styles.module.css';

export default () => {
    const [money, setMoney] = useState(3000);
    const [days, setDays] = useState(5);
    const { t } = useTranslation();

    const toReturn = Math.round(money + (money * (0.01 * days)));
    const returnDate = Moment().add(days, 'days').format('DD.MM.YYYY');

    return (
        <div className={`d-flex align-items-center flex-column bg-white rounded-lg w-100 h-100 ${styles.container}`}>
            <span className={`${styles['secondary-text']} mb-3`}>
                {t('calc.title1')}
                <span className="brand-text">
                    {' '}
                    {t('calc.title2')}
                </span>
                !
            </span>
            <div className="d-flex justify-content-between w-100 mb-3">
                <span className={`${styles['slider-header']}`}>{t('calc.want')}</span>
                <span className={`${styles['slider-header']} brand-text`}>
                    {money}
                    {' '}
                    рублей
                </span>
            </div>
            <input
                onChange={(event) => setMoney(event.target.valueAsNumber)}
                type="range"
                min="3000"
                max="10000"
                step="100"
                defaultValue="3000"
                className={`${styles.slider} mb-4`}
            />
            <div className="d-flex justify-content-between w-100 mb-3">
                <span className={`${styles['slider-header']}`}>{t('calc.time')}</span>
                <span className={`${styles['slider-header']} brand-text`}>
                    {days}
                    {' '}
                    {t('calc.timeunit')}
                </span>
            </div>
            <input
                onChange={(event) => setDays(event.target.valueAsNumber)}
                type="range"
                min="5"
                max="30"
                step="1"
                defaultValue="5"
                className={`${styles.slider} mb-4`}
            />
            <div className="d-flex flex-column w-100 mb-4">
                <span className={`${styles['secondary-text']}`}>
                    {t('calc.return')}
                    <span className={`${styles['slider-header']}`}>
                        {` ${toReturn} рублей`}
                    </span>
                </span>
                <span className={`${styles['secondary-text']}`}>
                    {t('calc.before')}
                    <span className={`${styles['slider-header']}`}>
                        {` ${returnDate}`}
                    </span>
                </span>
            </div>
            <button className={`${styles['accept-button']}`}>
                <span className={`${styles['accept-text']}`}>{t('getmoney')}</span>
            </button>
        </div>
    );
};
