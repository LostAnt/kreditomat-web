import Link from 'next/link';

import Documents from 'constants/documents';

import styles from './styles.module.css';

const documents = Documents();

const DocumentLink = ({ text, url }) => (
    <Link passHref href={url}>
        <a target="_blank" className={`${styles.text}`}>{text}</a>
    </Link>
);

export default () => {
    const docsDom = documents.map((document, index) => (
        <DocumentLink key={document.doc} text={`${index + 1}. ${document.text}`} url={`documents/${document.doc}`} />
    ));

    return (
        <div className="d-flex flex-column w-100 h-100">
            {docsDom}
        </div>
    );
};
