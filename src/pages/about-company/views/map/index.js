import {
    withScriptjs, withGoogleMap, GoogleMap, Marker
} from 'react-google-maps';
import { compose, withProps } from 'recompose';

import { getMapPoint } from 'utils/contacts';

export default compose(
    withProps({
        googleMapURL: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBsO9dDK3dBryIYu2UpulzQ7VxBDFxAR10&v=3.exp&libraries=geometry,drawing,places',
        loadingElement: <div style={{ height: '100%' }} />,
        containerElement: <div style={{ height: '400px', width: '100%' }} />,
        mapElement: <div style={{ height: '100%' }} />,
    }),
    withScriptjs,
    withGoogleMap
)(() => {
    const [mapPoint, setMapPoint] = React.useState(getMapPoint());

    React.useEffect(() => {
        setMapPoint(getMapPoint());
    }, []);

    return (
        <GoogleMap
            defaultZoom={15}
            defaultCenter={{ lat: mapPoint.lat, lng: mapPoint.long }}
        >
            <Marker position={{ lat: mapPoint.lat, lng: mapPoint.long }} />
        </GoogleMap>
);
});
