import { useTranslation } from 'react-i18next';

import Layout from 'components/layout';
import Container from 'components/container';

import { getAddress, getPhone } from 'utils/contacts';

import DocumentList from './views/document-list';
import Map from './views/map';


import styles from './styles.module.css';

export default () => {
    const { t } = useTranslation();

    const [phone, setPhone] = React.useState('');
    const [address, setAddress] = React.useState('');

    React.useEffect(() => {
        setPhone(getPhone());
        setAddress(getAddress());
    }, []);

    return (
        <Layout>
            <Container>
                <p className={`${styles['block-header']} w-100 text-center`}>{t('about.title')}</p>
                <div className="d-flex mb-5">
                    <div className="w-50 p-4">
                        {/* <img src="images/vvp.jpg" className={`${styles['photo']} mb-5`} /> */}
                        <p className="text-center">
                            {t('about.desc')}
                        </p>
                    </div>
                    <div className={`w-50 p-4 mt-4 position-relative ${styles['docs-container']}`}>
                        <span className={`${styles['docs-title']}`}>{t('about.docs.title')}</span>
                        <DocumentList />
                    </div>
                </div>
            </Container>
            <div className="mb-3">
                <Map />
            </div>
            <Container>
                <div className="w-100 d-flex justify-content-center mb-4">
                    <div className={`w-100 p-4 mt-4 position-relative ${styles['docs-container']}`}>
                        <span className={`${styles['contacts-title']}`}>{t('about.contacts.title')}</span>
                        <p>
                            {t('about.contacts.address')}
                            :
                            {' '}
                            {address}
                            {/* : г. Ташкент, Яккасарайский район, ул. Яккачинор, д. 4, кв. 54 */}
                        </p>
                        <p>
                            {t('about.contacts.worktime')}
                            : c 9:00 - 18:00 ежедневно
                        </p>
                        <p>
                            {t('about.contacts.phone')}
                            :
                            {' '}
                            {phone}
                            {/* : +998 981272381 */}
                        </p>
                        <p>
                            {t('about.contacts.email')}
                            : info@kreditomat24.ru
                            {/* : info@kreditomat24.uz */}
                        </p>
                    </div>
                </div>
            </Container>
        </Layout>
    );
};
