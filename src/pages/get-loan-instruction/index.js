import { useTranslation } from 'react-i18next';

import Layout from 'components/layout';
import Container from 'components/reading-container';

import styles from './styles.module.css';

export default () => {
    const { t } = useTranslation();

    return (
        <Layout>
            <Container>
                <p className={`${styles['block-header']} w-100 text-center`}>{t('howget.title')}</p>
                <p className={`${styles['step-title']}`}>{t('howget.subtitle')}</p>
                <div className="ml-4">
                    <p className={`${styles.text}`}>{t('howget.demands')}</p>
                    <p className={`m-0 ${styles.text}`}>
                        1.
                        {t('howget.demand1')}
                    </p>
                    <p className={`${styles.text}`}>
                        2.
                        {t('howget.demand2')}
                    </p>
                    <p className={`${styles.text}`}>{t('howget.3steps.title')}</p>
                </div>
                <p className={`${styles['step-title']}`}>{t('howget.step1.title')}</p>
                <p className={`w-75 ml-4 ${styles.text}`}>{t('howget.step1.desc')}</p>
                <div className="d-flex mb-4">
                    <div className="d-flex flex-column align-items-center" style={{ marginRight: 100 }}>
                        <p className={`${styles['substep-title']} mb-4`}>{t('howget.step1.substep1.title')}</p>
                        <img src="images/instruction-screen-1.png" className={`${styles['mobile-screen']} mr-3`} />
                    </div>
                    <div className="d-flex flex-column align-items-center">
                        <p className={`${styles['substep-title']} mb-4`}>{t('howget.step1.substep2.title')}</p>
                        <img src="images/instruction-screen-2.png" className={`${styles['mobile-screen']}`} />
                    </div>
                </div>
                <p className={`${styles['step-title']} mb-4`}>{t('howget.step2.title')}</p>
                <div className="d-flex mb-4 ml-4">
                    <div>
                        <img src="images/instruction-screen-3.png" className={`${styles['mobile-screen']} mr-3 pl-2`} />
                    </div>
                    <div className="d-flex flex-column">
                        <h5>{t('howget.step2.substep1.title')}</h5>
                        <p className={`w-100 ${styles.text}`}>
                            {t('howget.step2.substep1')}
                        </p>
                        <h5>{t('howget.step2.substep2.title')}</h5>
                        <p className={`w-100 ${styles.text}`}>
                            {t('howget.step2.substep2')}
                        </p>
                        <h5>{t('howget.step2.substep3.title')}</h5>
                        <p className={`w-100 ${styles.text}`}>
                            {t('howget.step2.substep3')}
                        </p>
                    </div>
                </div>
                <p className={`${styles['step-title']} mb-4`}>{t('howget.step3.title')}</p>
                <div className="d-flex mb-4 ml-4">
                    <div>
                        <img src="images/instruction-screen-4.png" className={`${styles['code-screen']} mr-3`} />
                    </div>
                    <div className="d-flex flex-column">
                        <p className={`w-100 ${styles.text}`}>
                            {t('howget.step3.desc')}
                        </p>
                    </div>
                </div>
                <p className={`${styles['step-title']}`}>{t('howget.terminal.subtitle')}</p>
                <p className={`${styles['step-title']}`}>{t('howget.terminal.step1.title')}</p>
                <p className={`w-75 ml-4 ${styles.text}`}>{t('howget.terminal.step1.desc')}</p>
                <div className="d-flex mb-4">
                    <div className="d-flex flex-column align-items-center" style={{ marginRight: 100 }}>
                        <p className={`${styles['substep-title']} mb-4`}>{t('howget.terminal.step1.sub1.title')}</p>
                        <img src="images/get-instruction/sub1-1.png" className={`${styles['terminal-screen']} mr-3`} />
                    </div>
                    <div className="d-flex flex-column align-items-center">
                        <p className={`${styles['substep-title']} mb-4`}>{t('howget.terminal.step1.sub2.title')}</p>
                        <img src="images/get-instruction/sub1-2.png" className={`${styles['terminal-screen']}`} />
                    </div>
                </div>
                <p className={`${styles['step-title']}`}>{t('howget.terminal.step2.title')}</p>
                <p className={`w-75 ml-4 ${styles.text}`}>{t('howget.terminal.step2.desc')}</p>
                <div className="d-flex mb-4">
                    <div className="d-flex flex-column align-items-center" style={{ marginRight: 100 }}>
                        <p className={`${styles['substep-title']} mb-4`}>{t('howget.terminal.step2.sub1.title')}</p>
                        <img src="images/get-instruction/sub2-1.png" className={`${styles['terminal-screen']} mr-3`} />
                    </div>
                    <div className="d-flex flex-column align-items-center">
                        <p className={`${styles['substep-title']} mb-4`}>{t('howget.terminal.step2.sub2.title')}</p>
                        <img src="images/get-instruction/sub2-2.png" className={`${styles['terminal-screen']}`} />
                    </div>
                </div>
                <div className="d-flex mb-4">
                    <div className="d-flex flex-column align-items-center" style={{ marginRight: 100 }}>
                        <p className={`${styles['substep-title']} mb-4`}>{t('howget.terminal.step2.sub3.title')}</p>
                        <img src="images/get-instruction/sub2-3.png" className={`${styles['terminal-screen']} mr-3`} />
                    </div>
                    <div className="d-flex flex-column align-items-center">
                        <p className={`${styles['substep-title']} mb-4`}>{t('howget.terminal.step2.sub4.title')}</p>
                        <img src="images/get-instruction/sub2-4.png" className={`${styles['terminal-screen']}`} />
                    </div>
                </div>
                <p className={`${styles['step-title']}`}>{t('howget.terminal.step3.title')}</p>
                <p className={`w-75 ml-4 ${styles.text} mb-5`}>{t('howget.terminal.step3.desc')}</p>
            </Container>
        </Layout>
    );
};
