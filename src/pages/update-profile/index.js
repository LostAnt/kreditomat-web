import React from 'react';
import Router from 'next/router';
import {
 Formik, Form, Field, ErrorMessage
} from 'formik';
import Moment from 'moment';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchUser, resetUser } from 'actions/user';
import { refreshTokens } from 'actions/auth';

import Layout from 'components/layout';
import Container from 'components/container';

import { api } from 'utils/apiClient';

import styles from './styles.module.css';

class UpdateProfile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false
        };
    }

    componentDidMount() {
        this.props.actions.refreshTokens(localStorage.authToken, localStorage.refreshToken)
        .then(() => {
            this.props.actions.fetchUser();
        }).catch(() => {
            Router.push('/auth');
        });
    }

    componentDidUpdate() {
        if (this.props.profile
            && this.props.profile.borrower
            && this.props.profile.borrower.isChecked) {
            Router.push('/profile');
        }
    }

    componentWillUnmount() {
        this.props.actions.resetUser();
    }

    MyField = ({ field, form, ...props }) => <input className="form-control" {...field} {...props} />;

    MyCheckboxField = ({ field, form, ...props }) => <input type="checkbox" className="form-control" {...field} {...props} />;

    MyFileField = ({ field, form, ...props }) => <input type="file" className="form-control mb-1" {...field} {...props} />;

    formatDateToServer = (dateString) => {
        if (!dateString) {
            return null;
        }

        const pattern = /(\d{2})\.(\d{2})\.(\d{4})/;
        const dt = new Date(dateString.replace(pattern, '$3-$2-$1'));

        const result = Moment(dt.getTime()).format('MM/DD/YYYY');

        return result;
    }

    fileToBytes = (file) => new Promise((resolve, reject) => {
            if (!file) {
                return resolve(null);
            }

            const reader = new FileReader();
            const fileByteArray = [];

            reader.readAsArrayBuffer(file);
            reader.onloadend = function (evt) {
                if (evt.target.readyState === FileReader.DONE) {
                    const arrayBuffer = evt.target.result;
                    const array = new Uint8Array(arrayBuffer);

                    for (let i = 0; i < array.length; i++) {
                        fileByteArray.push(array[i]);
                    }

                    resolve(fileByteArray);
                }
            };
        })

    handleSubmit = async (values) => {
        const PassportScan = await this.fileToBytes(values.passportScan);
        const PassportRegistrationScan = await this.fileToBytes(values.passportRegistrationScan);
        const IncomeStatementScan = await this.fileToBytes(values.incomeStatementScan);
        const FaceWithPassportPhoto = await this.fileToBytes(values.personPhoto);

        const person = {
            FirstName: values.firstName,
            LastName: values.lastName,
            MiddleName: values.middleName,
            BirthDate: this.formatDateToServer(values.birthDate),
            PassportSerial: values.passportSerial,
            PassportNumber: values.passportNumber,
            PassportIssuer: values.passportIssuer,
            PassportIssueDate: this.formatDateToServer(values.passportIssueDate),
            BirthPlace: values.birthPlace,
            Gender: values.gender,
            Inn: values.inn,
            Iin: values.iin,
            Email: values.email,
            PassportScan,
            PassportRegistrationScan,
            IncomeStatementScan,
            FaceWithPassportPhoto
        };

        let borrower = {
            CountryRegistration: values.countryRegistration,
            CityRegistration: values.cityRegistration,
            StreetRegistration: values.streetRegistration,
            BuildingNumberRegistration: values.buildingNumberRegistration,
            BuildingBlockRegistration: values.buildingBlockRegistration,
            ApartmentNumberRegistration: values.apartmentNumberRegistration
        };

        let livingParams = {
            Country: values.country,
            City: values.city,
            Street: values.street,
            BuildingNumber: values.houseNumber,
            BuildingBlock: values.houseBlock,
            ApartmentNumber: values.apartment
        };

        if (values.sameAddress) {
            livingParams = {
                Country: borrower.CountryRegistration,
                City: borrower.CityRegistration,
                Street: borrower.StreetRegistration,
                BuildingNumber: borrower.BuildingNumberRegistration,
                BuildingBlock: borrower.BuildingBlockRegistration,
                ApartmentNumber: borrower.ApartmentNumberRegistration
            };
        }

        const relative = {
            FirstName: values.relativeFirstName,
            LastName: values.relativeLastName,
            RelationTypeId: parseInt(values.relativeType),
            PersonTypeId: 0,
            PhoneNumber: values.relativePhone,
            Address: values.relativeAddress,
            Id: values.relativeId,
        };

        const friend = {
            FirstName: values.friendFirstName,
            LastName: values.friendLastName,
            RelationTypeId: 0,
            PersonTypeId: 0,
            PhoneNumber: values.friendPhone,
            Address: values.friendAddress,
            Id: values.friendId,
        };

        const chief = {
            FirstName: values.chiefFirstName,
            LastName: values.chiefLastName,
            RelationTypeId: 0,
            PersonTypeId: 0,
            PhoneNumber: values.chiefPhone,
            Address: values.chiefAddress,
            Id: values.chiefId,
        };

        borrower = {
            ...borrower,
            ...livingParams,
            relative,
            friend,
            chief
        };

        this.setState({ isLoading: true });

        api.post('borrower/profile', { person, borrower }).then(() => {
            this.setState({ isLoading: false });
            Router.push('/profile');
        }).catch((error) => {

        });
    }

    render() {
        if (!this.props.isLoaded || this.state.isLoading) {
            return (
                <Layout>
                    <div className={`d-flex align-items-center justify-content-center ${styles['spinner-container']}`}>
                        <div className="spinner-border" style={{ width: '3rem', height: '3rem' }} role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    </div>
                </Layout>
            );
        }

        const { profile } = this.props;
        const { borrower } = profile;

        const initialValues = { ...profile.person, ...profile.borrower };

        initialValues.birthDate = Moment(new Date(initialValues.birthDate)).format('DD.MM.YYYY');
        initialValues.passportIssueDate = Moment(new Date(initialValues.passportIssueDate)).format('DD.MM.YYYY');

        const email = initialValues.personEmails.find((item) => item.email.isActive);
        const emailAddress = email ? email.email.emailAddress : '';

        initialValues.email = emailAddress;

        if (borrower.countryRegistration === borrower.country
            && borrower.cityRegistration === borrower.city
            && borrower.streetRegistration === borrower.street
            && borrower.buildingNumberRegistration === borrower.buildingNumber
            && borrower.buildingBlockRegistration === borrower.buildingBlock
            && borrower.apartmentNumberRegistration === borrower.apartmentNumber) {
                initialValues.sameAddress = true;
        }

        initialValues.relativeType = '1';

        if (borrower.relative) {
            initialValues.relativeType = borrower.relative.relationTypeId;
            initialValues.relativeFirstName = borrower.relative.firstName;
            initialValues.relativeLastName = borrower.relative.lastName;
            initialValues.relativePhone = borrower.relative.phoneNumber;
            initialValues.relativeAddress = borrower.relative.address;
            initialValues.relativeId = borrower.relative.id;
        }

        if (borrower.friend) {
            initialValues.friendFirstName = borrower.friend.firstName;
            initialValues.friendLastName = borrower.friend.lastName;
            initialValues.friendPhone = borrower.friend.phoneNumber;
            initialValues.friendAddress = borrower.friend.address;
            initialValues.friendId = borrower.friend.id;
        }

        if (borrower.chief) {
            initialValues.chiefFirstName = borrower.chief.firstName;
            initialValues.chiefLastName = borrower.chief.lastName;
            initialValues.chiefPhone = borrower.chief.phoneNumber;
            initialValues.chiefAddress = borrower.chief.address;
            initialValues.chiefId = borrower.chief.id;
        }

console.log({ ...profile.person, ...profile.borrower });
        return (
            <Layout>
                <Container>
                    <Formik
                        enableReinitialize
                        initialValues={initialValues}
                        onSubmit={this.handleSubmit}
                    >
                        {({
                            values,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                            setFieldValue
                            /* and other goodies */
                        }) => (
                            <Form>
                                <div className="row mx-lg-n5">
                                    <div className="col py-2 px-lg-2 mr">
                                        <div className="rounded-lg shadow d-flex flex-column  p-3 h-100">
                                            <h4 className="pb-1 text-center">Паспорт</h4>
                                            <span className="font-weight-bold">Фамилия</span>
                                            <Field component={this.MyField} name="firstName" />
                                            <span className="font-weight-bold">Имя</span>
                                            <Field component={this.MyField} name="lastName" />
                                            <span className="font-weight-bold">Отчество</span>
                                            <Field component={this.MyField} name="middleName" />
                                            <span className="font-weight-bold">Дата рождения</span>
                                            <Field component={this.MyField} name="birthDate" />
                                            <span className="font-weight-bold">Серия паспорта</span>
                                            <Field component={this.MyField} name="passportSerial" />
                                            <span className="font-weight-bold">Номер паспорта</span>
                                            <Field component={this.MyField} name="passportNumber" />
                                            <span className="font-weight-bold">ИИН</span>
                                            <Field component={this.MyField} name="iin" />
                                            <span className="font-weight-bold">Кем выдан</span>
                                            <Field component={this.MyField} name="passportIssuer" />
                                            <span className="font-weight-bold">Код подразделения</span>
                                            <Field component={this.MyField} name="passportIssuerCode" />
                                            <span className="font-weight-bold">Дата выдачи</span>
                                            <Field component={this.MyField} name="passportIssueDate" />
                                            <span className="font-weight-bold">Место рождения</span>
                                            <Field component={this.MyField} name="birthPlace" />
                                            <h5 className="mt-3">Адрес прописки</h5>
                                            <span className="font-weight-bold">Страна</span>
                                            <Field component={this.MyField} name="countryRegistration" />
                                            <span className="font-weight-bold">Город</span>
                                            <Field component={this.MyField} name="cityRegistration" />
                                            <span className="font-weight-bold">Улица</span>
                                            <Field component={this.MyField} name="streetRegistration" />
                                            <span className="font-weight-bold">Номер дома</span>
                                            <Field component={this.MyField} name="buildingNumberRegistration" />
                                            <span className="font-weight-bold">Блок</span>
                                            <Field component={this.MyField} name="buildingBlockRegistration" />
                                            <span className="font-weight-bold">Квартира</span>
                                            <Field component={this.MyField} name="apartmentNumberRegistration" />
                                        </div>
                                    </div>
                                    <div className="col py-2 px-lg-2">
                                        <div className="rounded-lg shadow d-flex flex-column p-3 h-100">
                                            <h4 className="text-center">Контактная информация</h4>
                                            <span className="font-weight-bold">Пол</span>
                                            <Field className="form-control" as="select" name="gender">
                                                <option value="M">Мужской</option>
                                                <option value="F">Женский</option>
                                            </Field>
                                            <span className="font-weight-bold">Email</span>
                                            <Field component={this.MyField} name="email" />
                                            <span className="font-weight-bold">ИНН</span>
                                            <Field component={this.MyField} name="inn" />
                                            <h5 className="mt-3">Адрес проживания</h5>
                                            <div className="d-flex align-items-center">
                                                <label className="pr-2" htmlFor="sameAddress">Совпадает с пропиской</label>
                                                <Field component={this.MyCheckboxField} name="sameAddress" id="sameAddress" checked={values.sameAddress} />
                                            </div>
                                            {
                                                !values.sameAddress && (
                                                    <div className="d-flex flex-column" id="addressLiving">
                                                        <span className="font-weight-bold">Страна</span>
                                                        <Field component={this.MyField} name="country" />
                                                        <span className="font-weight-bold">Город</span>
                                                        <Field component={this.MyField} name="city" />
                                                        <span className="font-weight-bold">Улица</span>
                                                        <Field component={this.MyField} name="street" />
                                                        <span className="font-weight-bold">Номер дома</span>
                                                        <Field component={this.MyField} name="buildingNumber" maxLength="4" />
                                                        <span className="font-weight-bold">Блок</span>
                                                        <Field component={this.MyField} name="buildingBlock" maxLength="4" />
                                                        <span className="font-weight-bold">Квартира</span>
                                                        <Field component={this.MyField} name="apartmentNumber" maxLength="6" />
                                                    </div>
                                                )
                                            }
                                        </div>
                                    </div>
                                </div>
                                <div className="row mx-lg-n5">
                                    <div className="col py-2 px-lg-2">
                                        <div className="rounded-lg shadow d-flex flex-column p-3 h-100">
                                            <h4 className="text-center">Документы</h4>
                                            <span className="font-weight-bold">Скан главной страницы паспорта</span>
                                            {!profile.person.personPhotos.find((i) => i.fileTypeId === 1) ? null
                                                : (<span className="font-weight-bold text-success">Добавлен</span>)}
                                            <input
                                                id="passportScan"
                                                name="passportScan"
                                                type="file"
                                                onChange={(event) => {
                                                    setFieldValue('passportScan', event.currentTarget.files[0]);
                                                }}
                                                className="form-control"
                                            />
                                            <span className="font-weight-bold">Скан страницы регистрации паспорта</span>
                                            {!profile.person.personPhotos.find((i) => i.fileTypeId === 5) ? null
                                                : (<span className="font-weight-bold text-success">Добавлен</span>)}
                                            <input
                                                id="passportRegistrationScan"
                                                name="passportRegistrationScan"
                                                type="file"
                                                onChange={(event) => {
                                                    setFieldValue('passportRegistrationScan', event.currentTarget.files[0]);
                                                }}
                                                className="form-control"
                                            />
                                            <span className="font-weight-bold">Скан справки о доходах</span>
                                            {!profile.person.personPhotos.find((i) => i.fileTypeId === 6) ? null
                                                : (<span className="font-weight-bold text-success">Добавлен</span>)}
                                            <input
                                                id="incomeStatementScan"
                                                name="incomeStatementScan"
                                                type="file"
                                                onChange={(event) => {
                                                    setFieldValue('incomeStatementScan', event.currentTarget.files[0]);
                                                }}
                                                className="form-control"
                                            />
                                            <span className="font-weight-bold">Ваше фото с паспортом и картой в руках</span>
                                            {!profile.person.personPhotos.find((i) => i.fileTypeId === 3) ? null
                                                : (<span className="font-weight-bold text-success">Добавлен</span>)}
                                            <input
                                                id="personPhoto"
                                                name="personPhoto"
                                                type="file"
                                                onChange={(event) => {
                                                    setFieldValue('personPhoto', event.currentTarget.files[0]);
                                                }}
                                                className="form-control"
                                            />
                                            {
                                                console.log(values)
                                            }
                                        </div>
                                    </div>
                                </div>
                                <h3 className="text-center pt-2">Дополнительные контакты</h3>
                                <div className="row mx-lg-n5">
                                    <div className="col py-2 px-lg-2 mr">
                                        <div className="rounded-lg shadow d-flex flex-column p-3 h-100">
                                            <h4 className="text-center">Родственник</h4>
                                            <span className="font-weight-bold">Кем приходится</span>
                                            <Field className="form-control" as="select" name="relativeType">
                                                <option value="1">Родитель</option>
                                                <option value="2">Брат/Сестра</option>
                                                <option value="3">Супруг/Супруга</option>
                                            </Field>
                                            <span className="font-weight-bold">Имя</span>
                                            <Field component={this.MyField} name="relativeFirstName" />
                                            <span className="font-weight-bold">Фамилия</span>
                                            <Field component={this.MyField} name="relativeLastName" />
                                            <span className="font-weight-bold">Номер телефона</span>
                                            <Field component={this.MyField} name="relativePhone" />
                                            <span className="font-weight-bold">Адрес</span>
                                            <Field component={this.MyField} name="relativeAddress" />
                                        </div>
                                    </div>
                                    <div className="col py-2 px-lg-2 mr">
                                        <div className="rounded-lg shadow d-flex flex-column p-3 h-100">
                                            <h4 className="text-center">Друг/Подруга</h4>
                                            <span className="font-weight-bold">Имя</span>
                                            <Field component={this.MyField} name="friendFirstName" />
                                            <span className="font-weight-bold">Фамилия</span>
                                            <Field component={this.MyField} name="friendLastName" />
                                            <span className="font-weight-bold">Номер телефона</span>
                                            <Field component={this.MyField} name="friendPhone" />
                                            <span className="font-weight-bold">Адрес</span>
                                            <Field component={this.MyField} name="friendAddress" />
                                        </div>
                                    </div>
                                    <div className="col py-2 px-lg-2 mr">
                                        <div className="rounded-lg shadow d-flex flex-column p-3 h-100">
                                            <h4 className="text-center">Руководитель</h4>
                                            <div className="col py-2 px-lg-2">
                                                <span className="font-weight-bold">Имя</span>
                                                <Field component={this.MyField} name="chiefFirstName" />
                                                <span className="font-weight-bold">Фамилия</span>
                                                <Field component={this.MyField} name="chiefLastName" />
                                                <span className="font-weight-bold">Номер телефона</span>
                                                <Field component={this.MyField} name="chiefPhone" />
                                                <span className="font-weight-bold">Адрес</span>
                                                <Field component={this.MyField} name="chiefAddress" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="d-flex justify-content-center">
                                    <button type="submit" className="btn rounded-pill button-brand-color mb-2 mt-2 p-3">Сохранить</button>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </Container>
            </Layout>
        );
    }
}

const mapStateToProps = (state) => ({
    profile: state.user.profile,
    isLoading: state.user.isLoading,
    isLoaded: state.user.isLoaded
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({
        fetchUser,
        resetUser,
        refreshTokens
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(UpdateProfile);
