
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Router from 'next/router';
import Link from 'next/link';

import { auth } from 'actions/auth';

import Layout from 'components/layout';
import Container from 'components/container';

import { api } from 'utils/apiClient';

import styles from './styles.module.css';

class Profile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            login: '',
            code: '',
            error: null,
            successRequest: null,
            success: null
        };
    }

    componentDidMount() {
        if (this.props.token) {
            Router.push('/profile');
        }
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.token && this.props.token) {
            Router.push('/profile');
        }
    }

    handleAuthSubmit = () => {
        if (this.state.successRequest) {
            return;
        }

        api.post('users/restorerequest', { login: this.state.login }).then((response) => {
            this.setState({ successRequest: 'Код восстановления отправлен в SMS' });
        }).catch((error) => {
            if (error && error.error && error.error.response && error.error.response.data) {
                this.setState({ error: error.error.response.data });
            } else {
                this.setState({ error: 'Ошибка сервера. Повторите попытку позже.' });
            }
        });
    }

    handleAcceptCode = () => {
        if (this.state.success) {
            return;
        }

        api.post('users/restore', { login: this.state.login, code: this.state.code }).then((response) => {
            this.setState({ success: 'Ваш новый пароль отправлен в SMS' });
        }).catch((error) => {
            if (error && error.error && error.error.response && error.error.response.data) {
                if (error.error.response.data.indexOf('Code already expired') !== -1) {
                    this.setState({ error: 'Время действия кода истекло. Получите новый.' });
                    return;
                }

                this.setState({ error: error.error.response.data });
                return;
            }

            this.setState({ error: 'Непредвиденная ошибка' });
        });
    }

    handleAuthLoginChange = (event) => {
        this.setState({ login: event.target.value, error: null });
    }

    handleCodeChange = (event) => {
        this.setState({ code: event.target.value });
    }

    render() {
        return (
            <Layout>
                <Container>
                    <div className="d-flex align-items-center justify-content-center w-100 h-100">
                        <div className="d-flex flex-column align-items-center justify-content-center w-100 mb-4 w-25">
                            <p className={`${styles.header}`}>Восстановление пароля</p>
                            {
                                !this.state.successRequest ? (
                                    <>
                                        <p className={`${styles.label} mb-1`}>Номер телефона:</p>
                                        <input
                                            id="123"
                                            onChange={this.handleAuthLoginChange}
                                            className={`${styles.input} mb-3`}
                                            type="number"
                                        />
                                        <button onClick={this.handleAuthSubmit} type="button" className="btn rounded-pill mb-md-1 button-brand-color pl-5 pr-5 pb-2 pt-2">Получить код</button>
                                    </>
                                ) : (
                                    <>
                                        <p className={`${styles.label} mb-1`}>Код восстановления:</p>
                                        <input
                                            id="321"
                                            onChange={this.handleCodeChange}
                                            className={`${styles.input} mb-3`}
                                            type="number"
                                            value={this.state.code}
                                        />
                                        <button onClick={this.handleAcceptCode} type="button" className="btn rounded-pill mb-md-1 button-brand-color pl-5 pr-5 pb-2 pt-2">Восстановить</button>
                                    </>
                                )
                            }
                            {
                                (this.state.success || this.state.successRequest)
                                    && (<p className="text-success pt-1">{this.state.success || this.state.successRequest}</p>)
                            }
                            {
                                this.state.error
                                    && (<p className="text-danger pt-1">{this.state.error}</p>)
                            }
                            <Link href="auth">
                                <a className="text-primary">Вход в аккаунт</a>
                            </Link>
                        </div>
                    </div>
                </Container>
            </Layout>
        );
    }
}

const mapStateToProps = (state) => ({
    token: state.auth.authToken
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({
        auth
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
