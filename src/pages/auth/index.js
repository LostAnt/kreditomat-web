
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Router from 'next/router';
import Link from 'next/link';

import { auth } from 'actions/auth';

import Layout from 'components/layout';
import Container from 'components/container';

import styles from './styles.module.css';

class Profile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            login: '',
            pass: '',
            error: null
        };
    }

    componentDidMount() {
        if (this.props.token) {
            Router.push('/profile');
        }
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.token && this.props.token) {
            Router.push('/profile');
        }
    }

    handleAuthSubmit = () => {
        if (!this.state.login || !this.state.pass) {
            this.setState({ error: 'Введите логин и пароль' });

            return;
        }

        this.props.actions.auth(this.state.login, this.state.pass).catch((error) => {
            if (error && error.error && error.error.response && error.error.response.data) {
                // if (error.error.response.data.indexOf('Code already expired') !== -1) {
                //     this.setState({ error: 'Время действия кода истекло. Получите новый.' });
                //     return;
                // }

                this.setState({ error: error.error.response.data });
            } else {
                this.setState({ error: 'Ошибка сервера. Повторите попытку позже.' });
            }
        });
    }

    handleAuthLoginChange = (event) => {
        this.setState({ login: event.target.value, error: null });
    }

    handleAuthPassChange = (event) => {
        this.setState({ pass: event.target.value, error: null });
    }

    render() {
        return (
            <Layout>
                <Container>
                    <div className="d-flex align-content-between justify-content-between flex-column h-100 w-100">
                        <div className="d-flex flex-column align-items-center justify-content-center w-100">
                            <img src="images/short_logo.png" className={`${styles.logo} mb-4`} />
                            <div className={`rounded-lg shadow d-flex ${styles.container} p-4`}>
                                <div className="pr-4 w-100 d-flex flex-column align-items-center">
                                    <p className={`${styles.label} mb-3`}>Авторизоваться</p>
                                    <div className="pb-3">
                                        <p className={`${styles.text} mb-1`}>Номер телефона:</p>
                                        <input
                                            onChange={this.handleAuthLoginChange}
                                            className={`${styles.text} mb-3`}
                                        />
                                        <p className={`${styles.text} mb-1`}>Пароль:</p>
                                        <input
                                            onChange={this.handleAuthPassChange}
                                            className={`${styles.text}`}
                                            type="password"
                                        />
                                    </div>
                                    <button onClick={this.handleAuthSubmit} type="button" className="text-light btn rounded-pill mb-md-1 button-brand-color pl-5 pr-5 pb-2 pt-2">Войти</button>
                                    <div>
                                        {
                                        this.state.error
                                            && (<p className="text-danger pt-1">{this.state.error}</p>)
                                    }
                                    </div>
                                    <Link href="restore">
                                        <a className="text-dark">Забыли пароль?</a>
                                    </Link>
                                </div>
                                <div className={`${styles.divider} h-100`} />
                                <div className={`pl-4 d-flex flex-column align-items-center ${styles.leftContainer}`}>
                                    <p className={`${styles.label} mb-5`}>Нет аккаунта?</p>
                                    <p className={`${styles.text} mb-4 text-center`}>Если у вас еще нет аккаунта, пожалуйста, зарегистрируйтесь.</p>
                                    <Link href="registration">
                                        <button type="button" className="text-light btn rounded-pill mb-md-1 button-brand-color pl-5 pr-5 pb-2 pt-2">Регистрация</button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </Container>
            </Layout>
        );
    }
}

const mapStateToProps = (state) => ({
    token: state.auth.authToken
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({
        auth
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
