import Map from 'src/pages/terminals/views/map';
import Contacts from 'src/pages/terminals/views/contacts';

export default () => (
    <>
        <Map />
        <Contacts />
    </>
);
