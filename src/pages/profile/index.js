import React from 'react';
import Router from 'next/router';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchUser, resetUser } from 'actions/user';
import { fetchActiveLoan, resetActiveLoan } from 'actions/active-loan';
import { fetchLoansHistory, resetLoansHistory } from 'actions/loans-history';
import { refreshTokens, logout } from 'actions/auth';

import Layout from 'components/layout';
import Container from 'components/container';

import { api } from 'utils/apiClient';

import ActiveLoan from './views/active-loan';
import PersonalData from './views/personal-data';
import Account from './views/account';
import Support from './views/support';
import LoanHistory from './views/loan-history';

import styles from './styles.module.css';

class Profile extends React.Component {
    componentDidMount() {
        this.props.actions.refreshTokens(localStorage.authToken, localStorage.refreshToken).then(() => {
            this.props.actions.fetchUser();
            this.props.actions.fetchActiveLoan();
            this.props.actions.fetchLoansHistory();
        }).catch(() => {
            Router.push('/auth');
        });
    }

    componentWillUnmount() {
        this.props.actions.resetUser();
        this.props.actions.resetActiveLoan();
        this.props.actions.resetLoansHistory();
    }

    handleLogout = () => {
        this.props.actions.logout();
    }

    base64ToArrayBuffer = (base64) => {
        const binaryString = window.atob(base64);
        const bytes = new Uint8Array(binaryString.length);

        return bytes.map((byte, i) => binaryString.charCodeAt(i));
    }

    createAndDownloadBlobFile = (body, filename, extension = 'docx') => {
        const blob = new Blob([body]);
        const fileName = `${filename}.${extension}`;

        if (navigator.msSaveBlob) {
            navigator.msSaveBlob(blob, fileName);
        } else {
            const link = document.createElement('a');

            if (link.download !== undefined) {
                const url = URL.createObjectURL(blob);

                link.setAttribute('href', url);
                link.setAttribute('download', fileName);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }

    handleDownloadFile = (creditId) => {
        api.get(`documents/document/offer/byCredit/${creditId}/`).then((response) => {
            const arrayBuffer = this.base64ToArrayBuffer(response.data);

            this.createAndDownloadBlobFile(arrayBuffer, 'Договор-Кредитомат24');
        });
    }

    render() {
        if (!this.props.isLoaded) {
            return (
                <Layout>
                    <div className={`d-flex align-items-center justify-content-center ${styles['spinner-container']}`}>
                        <div className="spinner-border" style={{ width: '3rem', height: '3rem' }} role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    </div>
                </Layout>
            );
        }

        return (
            <Layout>
                <Container>
                    <div className="row mx-lg-n5 ">
                        <div className="col py-2 px-lg-2 mr">
                            <ActiveLoan
                                loan={this.props.activeLoan}
                                emails={this.props.profile.person.personEmails}
                                onDownload={this.handleDownloadFile}
                            />
                        </div>
                        <div className="col py-2 px-lg-2 mr">
                            <Account
                                onLogout={this.handleLogout}
                            />
                        </div>
                        <div className="col-6 py-2 px-lg-2">
                            <Support />
                        </div>
                    </div>
                    <div className="row mx-lg-n5">
                        <div className="col py-2 px-lg-2 mr">
                            <LoanHistory
                                loans={this.props.loansHistory}
                                onDownload={this.handleDownloadFile}
                            />
                        </div>
                        <div className="col-9 py-2 px-lg-2">
                            <PersonalData
                                person={this.props.profile.person}
                                borrower={this.props.profile.borrower}
                            />
                        </div>
                    </div>
                </Container>
            </Layout>
        );
    }
}

const mapStateToProps = (state) => ({
    profile: state.user.profile,
    isLoading: state.user.isLoading || state.activeLoan.isLoading || state.loansHistory.isLoading,
    isLoaded: state.user.isLoaded && state.activeLoan.isLoaded && state.loansHistory.isLoaded,
    activeLoan: state.activeLoan.loan,
    loansHistory: state.loansHistory.loans
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({
        fetchUser,
        resetUser,
        fetchActiveLoan,
        resetActiveLoan,
        fetchLoansHistory,
        resetLoansHistory,
        refreshTokens,
        logout
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
