import Moment from 'moment';
import Link from 'next/link';
import { useTranslation } from 'react-i18next';

import buildAddress from 'utils/addressBuilder';

export default (props) => {
    const { person, borrower } = props;
    const { t } = useTranslation();

    if (!person && !borrower) {
        return null;
    }

    const birthDate = person.birthDate ? Moment(person.birthDate).format('DD.MM.YYYY') : '';
    const serial = `${person.passportSerial || ''} ${person.passportNumber || ''}`;
    const regAddress = buildAddress({
        country: borrower.countryRegistration,
        city: borrower.cityRegistration,
        street: borrower.streetRegistration,
        houseNumber: borrower.buildingNumberRegistration,
        block: borrower.buildingBlockRegistration,
        apartmentNumber: borrower.apartmentNumberRegistration
    });
    const email = person.personEmails.find((item) => item.email.isActive);
    const emailAddress = email ? email.email.emailAddress : '';
    const phone = person.personPhones.find((item) => item.phoneType.id === 3
                    && item.phoneNumber.isActive);
    const phoneNumber = phone ? phone.phoneNumber.number : '';
    const livAddress = buildAddress({
        country: borrower.country,
        city: borrower.city,
        street: borrower.street,
        houseNumber: borrower.buildingNumber,
        block: borrower.buildingBlock,
        apartmentNumber: borrower.apartmentNumber
    });
    const gender = person.gender === 'F' ? 'Женский' : 'Мужской';

    return (
        <div className="rounded-lg shadow d-flex flex-column align-items-center p-3 h-100">
            <h5 className="border-bottom pb-1 mb-3 border-brand-color text-center">{t('profile.personal.title')}</h5>
            <div className="row h-100 w-100 mb-3">
                <div className="col border d-flex flex-column p-3 mr-4 rounded-lg align-items-center">
                    <h6>{t('profile.personal.passport')}</h6>
                    <div className="h-100 w-100 d-flex flex-column">
                        <span className="font-weight-bold">
                            {t('profile.personal.firstName')}
                            :&nbsp;
                            <span className="font-weight-normal">{person.firstName || ''}</span>
                        </span>
                        <span className="font-weight-bold">
                            {t('profile.personal.lastName')}
                            :&nbsp;
                            <span className="font-weight-normal">{person.lastName || ''}</span>
                        </span>
                        <span className="font-weight-bold">
                            {t('profile.personal.middleName')}
                            :&nbsp;
                            <span className="font-weight-normal">{person.middleName || ''}</span>
                        </span>
                        <span className="font-weight-bold">
                            {t('profile.personal.birthDate')}
                            :&nbsp;
                            <span className="font-weight-normal">{birthDate}</span>
                        </span>
                        <span className="font-weight-bold">
                            {t('profile.personal.serial')}
                            :&nbsp;
                            <span className="font-weight-normal">{serial}</span>
                        </span>
                        <span className="font-weight-bold">
                            {t('profile.personal.iin')}
                            :&nbsp;
                            <span className="font-weight-normal">{person.iin || ''}</span>
                        </span>
                        <span className="font-weight-bold">
                            {t('profile.personal.passportIssuer')}
                            :&nbsp;
                            <span className="font-weight-normal">{person.passportIssuer || ''}</span>
                        </span>
                        <span className="font-weight-bold">
                            {t('profile.personal.regAddress')}
                            :&nbsp;
                            <span className="font-weight-normal">{regAddress}</span>
                        </span>
                        <span className="font-weight-bold">
                            {t('profile.personal.birthPlace')}
                            :&nbsp;
                            <span className="font-weight-normal">{person.birthPlace || ''}</span>
                        </span>
                    </div>
                </div>
                <div className="col border d-flex flex-column p-3 rounded-lg align-items-center">
                    <h6>{t('profile.personal.contactinfo')}</h6>
                    <div className="h-100 w-100 d-flex flex-column">
                        <span className="font-weight-bold">
                            {t('profile.personal.emailAddress')}
                            :&nbsp;
                            <span className="font-weight-normal">{emailAddress}</span>
                        </span>
                        <span className="font-weight-bold">
                            {t('profile.personal.phoneNumber')}
                            :&nbsp;
                            <span className="font-weight-normal">{phoneNumber}</span>
                        </span>
                        <span className="font-weight-bold">
                            {t('profile.personal.livAddress')}
                            :&nbsp;
                            <span className="font-weight-normal">{livAddress}</span>
                        </span>
                        <span className="font-weight-bold">
                            {t('profile.personal.gender')}
                            :&nbsp;
                            <span className="font-weight-normal">{gender}</span>
                        </span>
                        <span className="font-weight-bold">
                            {t('profile.personal.inn')}
                            :&nbsp;
                            <span className="font-weight-normal">{person.inn || ''}</span>
                        </span>
                    </div>
                </div>
            </div>
            {
                !borrower.isChecked
                && <Link href="update-profile"><button type="button" className="btn button-brand-color">{t('profile.personal.change')}</button></Link>
            }
        </div>
    );
};
