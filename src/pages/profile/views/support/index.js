import { useTranslation } from 'react-i18next';

import { getPhone } from 'utils/contacts';

import styles from './styles.module.css';

export default () => {
    const { t } = useTranslation();

    const [phone, setPhone] = React.useState('');

    React.useEffect(() => {
        setPhone(getPhone());
    }, []);

    return (
        <div className="rounded-lg shadow d-flex flex-column align-items-center p-3 h-100">
            <h5 className="border-bottom pb-1 border-brand-color text-center">{t('profile.tp.title')}</h5>
            <span className="text-center lead mb-1">
                {t('profile.tp.desc')}
            </span>
            <div className="d-flex align-items-center">
                <img src="images/viber_icon.png" className={`mr-2 ${styles['sup-image']}`} />
                <img src="images/ws_icon.png" className={`mr-4 ${styles['sup-image']}`} />
                <div className="d-flex flex-column">
                    <span className="pb-1 font-weight-bold text-center">{phone}</span>
                    <span className="pb-1 font-weight-bold text-center">info@kreditomat24.ru</span>
                </div>
            </div>
        </div>
);
};
