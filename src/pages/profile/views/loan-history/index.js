import Moment from 'moment';
import { useTranslation } from 'react-i18next';

export default (props) => {
    let loansList = null;
    const { t } = useTranslation();

    if (props.loans) {
        loansList = props.loans.map((item, index) => {
            const dateCreate = item.dateCreate ? Moment(item.dateCreate).format('DD.MM.YYYY') : '';

            return (
                <div className="d-flex flex-column border-bottom mb-1" key={Math.random()}>
                    <span className="font-weight-bold">
                        {t('profile.loanhistory.contractnumber')}
                        :
                        <span className="font-weight-normal">
                            {item.contractCode}
                        </span>
                    </span>
                    <span className="font-weight-bold">
                        {t('profile.loanhistory.amount')}
                        :
                        <span className="font-weight-normal">
                            {item.approvalSum}
                        </span>
                    </span>
                    <span className="font-weight-bold">
                        {t('profile.loanhistory.date')}
                        :
                        <span className="font-weight-normal">
                            {dateCreate}
                        </span>
                    </span>
                    <span className="pb-1 text-primary link" onClick={() => props.onDownload(item.id)}>{t('profile.activeloan.contract')}</span>
                </div>
            );
        });
    }

    if (!loansList || loansList.length === 0) {
        return (
            <div className="rounded-lg shadow d-flex flex-column align-items-center p-3 h-100">
                <h5 className="border-bottom pb-1 mb-3 border-brand-color text-center">{t('profile.loanhistory.title')}</h5>
                <div className="d-flex flex-column align-items-center p-3 h-100" id="noCreditHistoryBlock">
                    <p className="text-center pb-1">
                        {t('profile.loanhistory.noloan')}
                    </p>
                    <button type="button" className="btn rounded-pill mb-md-1 button-brand-color">{t('profile.activeloan.getloan')}</button>
                </div>
            </div>
        );
    }

    return (
        <div className="rounded-lg shadow d-flex flex-column align-items-center p-3 h-100">
            <h5 className="border-bottom pb-1 mb-3 border-brand-color text-center">{t('profile.loanhistory.title')}</h5>
            <div className="h-100 w-100" style={{ overflowY: 'scroll', maxHeight: '350px' }}>
                {loansList}
            </div>
        </div>
    );
};
