import Moment from 'moment';
import Modal from 'react-modal';
import Router from 'next/router';
import {
    Formik, Form, Field, ErrorMessage
   } from 'formik';
import * as Yup from 'yup';
import { useTranslation } from 'react-i18next';

import Loader from 'components/loader';

import { api } from 'utils/apiClient';

export default (props) => {
    const { loan } = props;
    const { t } = useTranslation();

    const [modalIsOpen, setIsOpen] = React.useState(false);

    const openModal = () => {
        setIsOpen(true);
    };

    const closeModal = () => {
        setIsOpen(false);
    };

    const handlePay = () => {
        const email = props.emails.find((item) => item.email.isActive);
        const emailAddress = email ? email.email.emailAddress : '';

        if (emailAddress.trim()) {
            Router.push('/pay-loan');

            return;
        }

        openModal();
    };

    const handleSaveEmail = (values, { setSubmitting }) => {
        const person = {
            email: values.email
        };

        api.post('borrower/profile', { person }).then(() => {
            setSubmitting(false);
            Router.push('/pay-loan');
        }).catch((error) => {

        });
    };

    const MyField = ({ field, form, ...props }) => <input className="form-control" {...field} {...props} />;
    const MyErrorField = (msg) => <p className="text-danger">{msg}</p>;


    if (!loan) {
        return (
            <div className="rounded-lg shadow d-flex flex-column align-items-center p-3 h-100">
                <h5 className="border-bottom pb-1 border-brand-color text-center">{t('profile.activeloan.title')}</h5>
                <div className="d-flex flex-column align-items-center" id="noAcriveCreditBlock">
                    <span className="text-center pb-1">
                        {t('profile.activeloan.noloan')}
                    </span>
                    <button type="button" className="btn rounded-pill mb-md-1 button-brand-color">{t('profile.activeloan.getloan')}</button>
                </div>
            </div>
        );
    }
    const loanDate = loan.calculateEstimatedRepaymentDate
        ? Moment(loan.calculateEstimatedRepaymentDate).format('DD.MM.YYYY') : '';

    return (
        <div className="rounded-lg shadow d-flex flex-column align-items-center p-3 h-100" id="root">
            <h5 className="border-bottom pb-1 border-brand-color text-center">{t('profile.activeloan.getloan')}</h5>
            <div className="d-flex flex-column align-items-center justify-content-center" id="activeCreditBlock">
                <div className="d-flex flex-column align-items-start">
                    <span className="font-weight-bold">
                        {t('profile.activeloan.remain')}
                        :
                        <span className="font-weight-normal">
                            {' '}
                            {loan.loanCostOnCurrentDate || ''}
                        </span>
                    </span>
                    <span className="pb-1 font-weight-bold">
                        {t('profile.activeloan.term')}
                        :
                        <span className="font-weight-normal">
                            {' '}
                            {loanDate}
                        </span>
                    </span>
                </div>
                <div className="d-flex flex-column h-100 justify-content-center">
                    <button type="button" className="btn rounded-pill mb-md-1 button-brand-color" onClick={() => handlePay()}>{t('profile.activeloan.payloan')}</button>
                    {/* <button type="button" className="btn rounded-pill button-brand-color">{t('profile.activeloan.prolongation')}</button> */}
                </div>
                <span className="text-primary link" id="activeCreditOferta" onClick={() => props.onDownload(loan.creditId)}>{t('profile.activeloan.contract')}</span>
            </div>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
                contentLabel="Example Modal"
                style={{
                    overlay: {
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center'
                    },
                    content: {
                        position: 'relative',
                        left: '0',
                        top: '0',
                        bottom: '0',
                        right: '0',
                        height: '250px',
                        width: '500px',
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center'
                    }
                }}
            >
                <h5 className="pt-2">{t('profile.fillEmail.title')}</h5>
                <p>{t('profile.fillEmail.desc')}</p>
                <Formik
                    initialValues={{ email: '' }}
                    onSubmit={handleSaveEmail}
                    validationSchema={
                        Yup.object().shape({
                            email: Yup.string()
                              .email(`${t('incorrect') } E-mail`)
                              .required(`${t('required') } E-mail`),
                          })
                    }
                >
                    {({
                            handleSubmit,
                            isSubmitting
                        }) => (
                            <Form className="d-flex flex-column ">
                                <Loader show={isSubmitting} />
                                <div>
                                    <Field component={MyField} name="email" />
                                    <ErrorMessage name="email" render={MyErrorField} />
                                </div>
                                <button type="button" className="mt-1 btn rounded-pill mb-md-1 button-brand-color" onClick={() => handleSubmit()}>Сохранить</button>
                            </Form>
                        )}
                </Formik>
            </Modal>
        </div>
    );
};
