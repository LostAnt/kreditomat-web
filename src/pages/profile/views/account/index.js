import { useTranslation } from 'react-i18next';

export default (props) => {
    const { t } = useTranslation();

    return (
        <div className="rounded-lg shadow d-flex flex-column align-items-center p-3 h-100">
            <h5 className="border-bottom pb-1 border-brand-color text-center">{t('profile.account.title')}</h5>
            <div className="d-flex flex-column h-100 justify-content-center">
                {/* <button type="button" className="btn rounded-pill button-secondary-color mb-1">{t('profile.account.changepass')}</button> */}
                <button type="button" className="btn rounded-pill button-secondary-color" id="exitButton" onClick={props.onLogout}>{t('profile.account.logout')}</button>
            </div>
        </div>
);
};
