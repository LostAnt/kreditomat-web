import store from 'store';
import { Provider } from 'react-redux';
import { compose } from 'recompose';
import Modal from 'react-modal';

import WithAuthStorage from 'hocs/with-auth-storage';
import WithRedux from 'hocs/with-redux';
import WithRefreshTokens from 'hocs/with-refresh-tokens';

import 'utils/i18nInitializer';

import 'bootstrap/dist/css/bootstrap.min.css';
import './styles.css';
import 'react-multi-carousel/lib/styles.css';

Modal.setAppElement('#root');

function App({ Component, pageProps }) {
    return (
        <Provider store={store}>
            <div id="root">
                <Component {...pageProps} />
            </div>
        </Provider>
    );
}

export default compose(
    WithRedux,
    WithRefreshTokens,
    WithAuthStorage
)(App);
