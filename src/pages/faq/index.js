import { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import Container from 'components/container';
import Layout from 'components/layout';
import Accordion from 'react-bootstrap/Accordion';

import Questions from 'constants/questions';

import styles from './styles.module.css';

export default () => {
    const { t } = useTranslation();

    const sections = Object.keys(Questions);
    const [selectedSection, setSelectedSection] = useState(sections[0]);

    return (
        <Layout>
            <Container>
                <p className={`${styles['block-header']} w-100 text-center`}>Вопрос-ответ</p>
                <div className="w-100 mb-4 d-flex">
                    <div className={`d-flex flex-column ${styles['section-container']} mr-5`}>
                        {
                            sections.map((section, index) => (
                                <p
                                    key={section}
                                    className={`${selectedSection === section ? styles['selected-section-title'] : styles['section-title']} m-0`}
                                    onClick={() => setSelectedSection(section)}
                                >
                                    {Questions[section].sectionTitle}
                                </p>
                            ))
                        }
                    </div>
                    <div className={`d-flex flex-column ${styles['questions-container']}`}>
                        <Accordion>
                            {
                                sections.map((section) => (
                                    <div className={`${section === selectedSection ? '' : 'd-none'}`} key={section}>
                                        {
                                            Questions[section].questions.map((question, qIndex) => (
                                                <div key={section}>
                                                    <Accordion.Toggle as="div" eventKey={section + qIndex}>
                                                        <div className={`${styles['q-header']}`}>
                                                            <div>{t(question.question)}</div>
                                                        </div>
                                                    </Accordion.Toggle>
                                                    <Accordion.Collapse eventKey={section + qIndex}>
                                                        <div className={`${styles['q-answer']}`}>
                                                            <div>{t(question.answer)}</div>
                                                        </div>
                                                    </Accordion.Collapse>
                                                </div>
                                            ))
                                            }
                                    </div>
                                    ))
                            }
                        </Accordion>
                    </div>
                </div>
            </Container>
        </Layout>
    );
};
