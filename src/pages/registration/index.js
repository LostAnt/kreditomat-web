
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Router from 'next/router';
import Link from 'next/link';

import { auth } from 'actions/auth';

import Layout from 'components/layout';
import Container from 'components/container';

import { api } from 'utils/apiClient';

import styles from './styles.module.css';

class Profile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            login: '',
            pass: '',
            error: null,
            success: null
        };
    }

    componentDidMount() {
        if (this.props.token) {
            Router.push('/profile');
        }
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.token && this.props.token) {
            Router.push('/profile');
        }
    }

    handleAuthSubmit = () => {
        if (this.state.success) {
            return;
        }

        api.post('users/register', { login: this.state.login }).then((response) => {
            this.setState({ success: 'Ваш пароль отправлен в SMS' });
        }).catch((error) => {
            if (error && error.error && error.error.response && error.error.response.data) {
                this.setState({ error: error.error.response.data });
            } else {
                this.setState({ error: 'Ошибка сервера. Повторите попытку позже.' });
            }
        });
    }

    handleAuthLoginChange = (event) => {
        this.setState({ login: event.target.value, error: null });
    }

    handleAuthPassChange = (event) => {
        this.setState({ pass: event.target.value });
    }

    render() {
        return (
            <Layout>
                <Container>
                    <div className="d-flex align-content-between justify-content-between flex-column h-100 w-100">
                        <div className="d-flex flex-column align-items-center justify-content-center w-100">
                            <img src="images/short_logo.png" className={`${styles.logo} mb-4`} />
                            <div className={`rounded-lg shadow d-flex ${styles.container} p-4`}>
                                <div className={`pr-4 d-flex flex-column align-items-center ${styles.leftContainer}`}>
                                    <p className={`${styles.label} mb-5`}>Уже зарегестрированы?</p>
                                    <p className={`${styles.text} mb-4 text-center`}>Если у вас уже есть аккаунт, вы можете авторизоваться тут</p>
                                    <Link href="auth">
                                        <button type="button" className="text-light btn rounded-pill mb-md-1 button-brand-color pl-5 pr-5 pb-2 pt-2">Войти</button>
                                    </Link>
                                </div>
                                <div className={`${styles.divider} h-100`}></div>
                                <div className="pl-4 w-100 d-flex flex-column align-items-center">
                                    <p className={`${styles.label} mb-5`}>Зарегистрироваться</p>
                                    <div>
                                        <p className={`${styles.text} mb-1`}>Номер телефона:</p>
                                        <input
                                            onChange={this.handleAuthLoginChange}
                                            className={`${styles.input} mb-3`}
                                            type="number"
                                        />
                                    </div>
                                    <button onClick={this.handleAuthSubmit} type="button" className="text-light btn rounded-pill mb-md-1 button-brand-color pl-5 pr-5 pb-2 pt-2">Регистрация</button>
                                    {
                                        this.state.success
                                            && (<p className="text-success pt-1">{this.state.success}</p>)
                                    }
                                    {
                                        this.state.error
                                            && (<p className="text-danger pt-1">{this.state.error}</p>)
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </Container>
            </Layout>
        );
    }
}

const mapStateToProps = (state) => ({
    token: state.auth.authToken
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({
        auth
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
