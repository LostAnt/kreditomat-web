import { useTranslation } from 'react-i18next';
import Modal from 'react-modal';

import Layout from 'components/layout';
import Container from 'components/container';

import BankInstruction from './views/bank';
import Prolongation from './views/prolongation';
import Terminal from './views/terminal';

import styles from './styles.module.css';

export default () => {
    const { t } = useTranslation();

    const [modalIsOpen, setIsOpen] = React.useState(false);
    const [modalContent, setModalContent] = React.useState(null);

    const openModal = () => {
        setIsOpen(true);
    };

    const closeModal = () => {
        setIsOpen(false);
    };

    const showModalWithPayType = (type) => {
        switch (type) {
            case 'bank':
                setModalContent(<BankInstruction />);
                openModal();

                break;

            case 'prol':
                setModalContent(<Prolongation />);
                openModal();

                break;

            case 'terminal':
                setModalContent(<Terminal />);
                openModal();

                break;

            default:
                break;
        }
    };

    return (
        <Layout>
            <Container>
                <div className="row">
                    <p className={`${styles['block-header']} w-100 text-center mb-4`}>{t('howpay.title')}</p>
                    <div className="col d-flex align-items-center justify-content-center"><p className={`text-center ${styles['line-title']}`}>{t('howpay.card')}</p></div>
                    <div className="col"><img src="images/payment-instruction/visa.png" className={`${styles.image}`} /></div>
                    <div className="col"><img src="images/payment-instruction/master.png" className={`${styles.image}`} /></div>
                    <div className="col"><div className={`${styles.image}`} /></div>
                    <div className="w-100 mb-4" />
                    <div className="col d-flex align-items-center justify-content-center"><p className={`text-center ${styles['line-title']}`}>{t('howpay.cash')}</p></div>
                    <div className="col"><img onClick={() => showModalWithPayType('terminal')} src="images/payment-instruction/terminal.png" className={`${styles.image}`} /></div>
                    <div className="col"><img onClick={() => showModalWithPayType('bank')} src="images/payment-instruction/bank.png" className={`${styles.image}`} /></div>
                    <div className="col"><img onClick={() => showModalWithPayType('prol')} src="images/payment-instruction/prolongation.png" className={`${styles.image}`} /></div>
                </div>
                <Modal
                    isOpen={modalIsOpen}
                    onRequestClose={closeModal}
                    style={{
                    overlay: {
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center'
                    },
                    content: {
                        position: 'relative',
                        left: '0',
                        top: '0',
                        bottom: '0',
                        right: '0',
                        height: '90%',
                        width: '70%',
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center'
                    }
                }}
                >
                    <img onClick={closeModal} src="images/close.png" className={`${styles.closeImage}`} />
                    {modalContent}
                </Modal>
            </Container>
        </Layout>
    );
};
