import { useTranslation } from 'react-i18next';

import styles from './styles.module.css';

export default () => {
    const { t } = useTranslation();

    return (
        <div>
            <div className="d-flex justify-content-between">
                <div className="mt-5">
                    <p className={`${styles.header}`}>{t('howpay.terminal.title')}</p>
                    <p className={`${styles.description}`}>{t('howpay.terminal.subtitle')}</p>
                </div>
                <img src="images/terminal_small.png" />
            </div>
            <p className={`${styles.step}`}>{t('howpay.terminal.s1')}</p>
            <img src="images/payment-instruction/terminal_s1.png" className={`${styles.image} mb-4`} />
            <p className={`${styles.step}`}>{t('howpay.terminal.s2')}</p>
            <img src="images/payment-instruction/terminal_s2.png" className={`${styles.image}`} />
        </div>
    );
};
