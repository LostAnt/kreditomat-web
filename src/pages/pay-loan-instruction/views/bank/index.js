import { useTranslation } from 'react-i18next';

import styles from './styles.module.css';

export default () => {
    const { t } = useTranslation();

    return (
        <div>
            <div className="d-flex justify-content-between">
                <div className="mt-5">
                    <p className={`${styles.header}`}>{t('howpay.bank.title')}</p>
                    <p className={`${styles.step}`}>{t('howpay.bank.subtitle')}</p>
                </div>
                <img src="images/method_bank.png" className={`${styles.image}`} />
            </div>
            <p className={`${styles.step} mb-5`}>{t('howpay.bank.desc')}</p>
        </div>
    );
};
