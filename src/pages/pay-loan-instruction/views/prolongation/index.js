import { useTranslation } from 'react-i18next';

import styles from './styles.module.css';

export default () => {
    const { t } = useTranslation();

    return (
        <div>
            <div className="d-flex justify-content-between">
                <div className="mt-5">
                    <p className={`${styles.header}`}>{t('howpay.prolongation.title')}</p>
                    <p className={`${styles.description}`}>{t('howpay.prolongation.subtitle')}</p>
                </div>
                <img src="images/prolongation.png" className={`${styles.image}`} />
            </div>
            <p className={`${styles.step}`}>{t('howpay.prolongation.s1')}</p>
            <p className={`${styles.step}`}>{t('howpay.prolongation.s2')}</p>
            <p className={`${styles.step}`}>{t('howpay.prolongation.s3')}</p>
            <p className={`${styles.step}`}>{t('howpay.prolongation.s4')}</p>
            <p className={`${styles.step}`}>{t('howpay.prolongation.s5')}</p>
        </div>
    );
};
