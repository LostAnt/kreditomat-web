/* eslint-disable no-underscore-dangle */
/* eslint-disable class-methods-use-this */

import set from 'lodash/set';
import axios from 'axios';
import Router from 'next/router';

import store from 'store';

// import { updateTokens } from 'actions/auth';

import Request from 'utils/request';
import LocalStorageService from 'utils/localstorageservice';

export class ApiClient {
    axiosInstance;

    requestInstance;

    isRefreshing = false;

    requestQueue = [];

    constructor() {
        this.axiosInstance = axios.create();

        // this.axiosInstance.interceptors.response.use(null, (error) => {
        //     if ((error && error.response && error.response.status === 401)
        //     || error.message.indexOf('401') !== -1) {
        //         if (this.isRefreshing) {
        //             return new Promise(function(resolve, reject) {
        //                 this.requestQueue.push({resolve, reject})
        //               }).then(token => {
        //                 originalRequest.headers['Authorization'] = 'Bearer ' + token;
        //                 return this.axiosInstance(originalRequest);
        //               }).catch(err => {
        //                 return Promise.reject(err);
        //               })
        //         }

        //         return this._handle401().then((token) => {
        //             this.isRefreshing = false;

        //             store.dispatch(updateTokens(token.token, token.refreshToken));

        //             localStorage.authToken = token.token;
        //             localStorage.refreshToken = token.refreshToken;

        //             error.config.headers.Authorization = 'Bearer ' + token.token;

        //             processQueue(null, token.token);

        //             return axios.request(error.config);
        //         }).catch(error => {
        //             this.processQueue(error, null);

        //             return Promise.reject(error);
        //         });
        //     }

        //     if (error.config.url.indexOf('/token/refresh') !== -1) {
        //         Router.push('/auth');
        //     }

        //     return Promise.reject(error);
        // });

        this.requestInstance = new Request(this.axiosInstance);
    }

    // processQueue(error, token = null) {
    //     this.requestQueue.forEach(prom => {
    //         if (error) {
    //             prom.reject(error);
    //         } else {
    //             prom.resolve(token);
    //         }
    //     })

    //     this.requestQueue = [];
    // };

    get(resource, query = {}, options = {}) {
        return this.requestInstance.requestGet(this._setApiUrl(resource), query, this._setHeaders(options));
    }

    post(resource, data = {}, query = {}, options = {}) {
        return this.requestInstance.requestPost(
            this._setApiUrl(resource), data, query, this._setHeaders(options)
        );
    }

    postWithFullResponse(resource, data = {}, query = {}, options = {}) {
        return this.requestInstance.requestPost(
            this._setApiUrl(resource), data, query, this._setHeaders(options), true
        );
    }

    patch(resource, data = {}, query = {}, options = {}) {
        return this.requestInstance.requestPatch(
            this._setApiUrl(resource), data, query, this._setHeaders(options)
        );
    }

    delete(resource, query = {}, options = {}) {
        return this.requestInstance.requestDelete(this._setApiUrl(resource), query, this._setHeaders(options));
    }

    _setHeaders(options) {
        const currentAuthToken = localStorage.authToken;

        if (currentAuthToken) {
            set(options, 'headers.Authorization', `Bearer ${currentAuthToken}`);
        }

        return options;
    }

    _setApiUrl(resource) {
        // const apiUrl = 'https://10.10.0.66:45455/';
        let apiUrl = '';

        switch (localStorage.city) {
            case 'spb':
                apiUrl = 'http://spbmock/';

                break;

            case 'vor':
                apiUrl = 'http://185.55.40.31:14899/';

                break;


            default:
                apiUrl = 'http://185.55.40.31:14899/';
        }

        return apiUrl + resource;
    }

    // _handle401() {
    //     const resource = 'token/refresh';
    //     const data = {
    //         token: localStorage.authToken,
    //         refreshToken: localStorage.refreshToken
    //     };

    //     this.isRefreshing = true;

    //     return this.requestInstance.requestPost(
    //         this._setApiUrl(resource), data
    //     );
    // }
}

export const api = new ApiClient();
