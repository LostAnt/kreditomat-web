export default (fields) => {
    let house = fields.houseNumber;

    if (fields.block) {
        house = `${house}/${fields.block}`;
    }

    const address = `${fields.country || ''}, г. ${fields.city || ''}, ул. ${fields.street || ''}, `
        + `д. ${house || ''}, кв. ${fields.apartmentNumber || ''}`;

    return address;
};
