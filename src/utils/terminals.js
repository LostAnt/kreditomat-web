export const getMapPoints = () => {
    const city = localStorage && localStorage.city ? localStorage.city : 'vor';
    let phone = '';

    switch (city) {
        case 'vor':
            phone = [
                {
                    lat: 51.7253636, long: 39.1623629
                },
                {
                    lat: 51.706014, long: 39.149176
                },
                {
                    lat: 51.666076, long: 39.191666
                }
            ];

            break;

        case 'spb':
            phone = [
                { lat: 59.851445, long: 30.349979 },
                { lat: 59.858642, long: 30.228462 }
            ];

            break;

        default:
            phone = '+7 (812) 627-14-50';

            break;
    }

    return phone;
};

export const getMapCenter = () => {
    const city = localStorage && localStorage.city ? localStorage.city : 'vor';
    let point = '';

    switch (city) {
        case 'vor':
            point = {
                lat: 51.6565797,
                long: 39.1949347
            };

            break;

        case 'spb':
            point = {
                lat: 59.9546654,
                long: 30.3199169
            };

            break;

        default:
            point = {
                lat: 51.6565797,
                long: 39.1949347
            };

            break;
    }

    return point;
};

export const getAddresses = () => {
    const city = localStorage && localStorage.city ? localStorage.city : 'vor';
    let phone = '';

    switch (city) {
        case 'vor':
            phone = [
                '394088, Воронеж, Антонова-Овсеенко, 41, магазин "Пятёрочка"',
                '394088, Воронеж, Генерала Лизюкова, 60, ТРЦ "Аксиома"',
                '394030, Воронеж, ул. Кольцовская, 35, ТРЦ "Галерея Чижова"'
            ];

            break;

        case 'spb':
            phone = [
                '196233, Санкт-Петербург, просп. Космонавтов, 45 магазин "Окей"',
                '198332, Санкт-Петербург, просп. Маршала Жукова, 31, корп. 1 магазин "Окей"'
            ];

            break;

        default:
            phone = [
                '394088, Воронеж, Антонова-Овсеенко, 41, магазин "Пятёрочка"',
                '394088, Воронеж, Генерала Лизюкова, 60, ТРЦ "Аксиома"',
                '394030, Воронеж, ул. Кольцовская, 35, ТРЦ "Галерея Чижова"'
            ];

            break;
    }

    return phone;
};
