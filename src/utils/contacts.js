export const getPhone = () => {
    const city = localStorage.city ? localStorage.city : 'vor';
    let phone = '';

    switch (city) {
        case 'vor':
            phone = '+7 (812) 627-14-50';

            break;

        case 'spb':
            phone = '+7 (812) 209-90-02';

            break;

        default:
            phone = '+7 (812) 627-14-50';

            break;
    }

    return phone;
};

export const getAddress = () => {
    const city = localStorage.city ? localStorage.city : 'vor';
    let address = '';

    switch (city) {
        case 'vor':
            address = '394035, Воронеж​, Большая Стрелецкая, 20Б, офис 1/3';

            break;

        case 'spb':
            address = '194044, Санкт-Петербург, ул. Чугунная, дом 14, литер Ж, помещение 2Н, офис 6';

            break;

        default:
            address = '394035, Воронеж​, Большая Стрелецкая, 20Б, офис 1/3';

            break;
    }

    return address;
};

export const getMapPoint = () => {
    const city = localStorage.city ? localStorage.city : 'vor';
    let address = {};

    switch (city) {
        case 'vor':
            address = {
                lat: 51.651847,
                long: 39.2080487
            };

            break;

        case 'spb':
            address = {
                lat: 59.971790,
                long: 30.353045
            };

            break;

        default:
            address = {
                lat: 51.651847,
                long: 39.2080487
            };

            break;
    }

    return address;
};
