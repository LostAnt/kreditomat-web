
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import Ru from 'constants/translations/ru';
import Uz from 'constants/translations/uz';
import Kz from 'constants/translations/kz';

i18n
  .use(initReactI18next)
  .init({
    lng: 'ru',
    resources: {
      ru: {
        translations: Ru
      },
      kz: {
        translations: Kz
      },
      uz: {
        translations: Uz
      }
    },
    fallbackLng: 'ru',
    debug: true,
    ns: ['translations'],
    defaultNS: 'translations',
    keySeparator: false,
    interpolation: {
      escapeValue: false,
      formatSeparator: ','
    },
    react: {
      wait: true
    }
  });

export default i18n;
