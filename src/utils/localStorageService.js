const LocalStorageService = (function () {
    let service;
    function getService() {
        if (!service) {
            service = this;
            return service;
        }
        return service;
    }
    function setToken(tokenObj) {
        localStorage.setItem('authToken', tokenObj.authToken);
        localStorage.setItem('refreshToken', tokenObj.refreshToken);
    }
    function getAccessToken() {
        return localStorage.getItem('authToken');
    }
    function getRefreshToken() {
        return localStorage.getItem('refreshToken');
    }
    function clearToken() {
        localStorage.removeItem('authToken');
        localStorage.removeItem('refreshToken');
    }
    return {
        getService,
        setToken,
        getAccessToken,
        getRefreshToken,
        clearToken
    };
}());

export default LocalStorageService;
