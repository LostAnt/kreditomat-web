/* eslint-disable no-param-reassign */

import strictUriEncode from 'strict-uri-encode';
import isUndefined from 'lodash/isUndefined';
import isObject from 'lodash/isObject';

export const extract = (str) => str.split('?')[1] || '';

export const parse = (str) => {
    if (typeof str !== 'string') {
        return {};
    }

    str = str.trim().replace(/^(\?|#|&)/, '');

    if (!str) {
        return {};
    }

    return str.split('&').reduce((ret, param) => {
        const parts = param.replace(/\+/g, ' ').split('=');

        let key;
        let val;

        try {
            key = decodeURIComponent(parts.shift());
            val = parts.length > 0 ? decodeURIComponent(parts.join('=')) : null;
        } catch (e) {
console.warn(e); // eslint-disable-line
        }

        if (!val || !key) {
            return ret;
        }

        // Если массив
        if (key.substring(key.length - 2) === '[]') {
            key = key.substring(0, key.length - 2);

            ret[key] = ret[key] || [];

            ret[key] = ret[key].concat(val);
        } else {
            ret[key] = val;
        }

        return ret;
    }, {});
};

export const stringify = (obj) => {
    if (!obj || !isObject(obj)) {
        return '';
    }

    const queryParameters = Object.keys(obj).reduce((state, key) => {
        if (!obj) {
            return state;
        }

        const value = obj[key];

        if (isUndefined(value)) {
            return state;
        }

        if (value === null) {
            state.push(strictUriEncode(key));

            return state;
        }

        if (Array.isArray(value)) {
            if (value.length === 0) {
                return state;
            }

            const arrayQuery = value.map((val) => `${strictUriEncode(key)}[]=${strictUriEncode(val)}`);

            state.push(arrayQuery.join('&'));

            return state;
        }

        const queryParam = `${strictUriEncode(key)}=${strictUriEncode(value)}`;

        state.push(queryParam);

        return state;
    }, []);

    return queryParameters.join('&');
};
