
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-promise-reject-errors */

import isError from 'lodash/isError';
import axios from 'axios';
import https from 'https';

import { stringify } from 'utils/query-string';

export default class Request {
    axiosInstance = axios.create();

    agent = new https.Agent({
        rejectUnauthorized: false
    });

    constructor(anotherAxiosInstance) {
        if (anotherAxiosInstance) {
            this.axiosInstance = anotherAxiosInstance;
        }
    }

    request = (url, options = {}, withFullResponse = false) => this.axiosInstance({
        url,
        httpsAgent: this.agent,
        on401: this.requestGet,
        ...options
    }).then(
        (response) => {
            if (response.status >= 400) {
                return Promise.reject(response);
            }

            const data = withFullResponse ? response : response.data;

            return Promise.resolve(data);
        },
        (error) => {
            if (isError(error)) {
                return Promise.reject({
                    errors: [
                        {
                            message: 'Непредвиденная ошибка',
                            code: 500
                        }
                    ],
                    error
                });
            }

            return Promise.reject(error);
        }
    );

    requestPost = (
        url, body, query, options = {}, withFullResponse = false
    ) => {
        if (query) {
            url = `${url}?${stringify(query)}`;
        }

        return this.request(
            url,
            {
                method: 'POST',
                data: body,
                ...options
            },
            withFullResponse
        );
    };

    requestGet = (url, query, options = {}) => {
        if (query) {
            url = `${url}?${stringify(query)}`;
        }

        return this.request(url, {
            method: 'GET',
            ...options
        });
    };

    requestDelete = (url, query, options = {}) => {
        if (query) {
            url = `${url}?${stringify(query)}`;
        }

        return this.request(url, {
            method: 'DELETE',
            ...options
        });
    };

    requestPatch = (url, body, query, options = {}) => {
        if (query) {
            url = `${url}?${stringify(query)}`;
        }

        return this.request(url, {
            method: 'PATCH',
            data: JSON.stringify(body),
            ...options
        });
    };
}
