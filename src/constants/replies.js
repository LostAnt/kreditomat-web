export default () => [
    {
        reply: 'Добрый день! Пользовался услугами kreditomat24.Заём много времени не занимает,буквально 3 минуты и деньги у меня в кармане.При продлении были вопросы,поддержка сработала оперативно,всё доходчиво объяснили.10/10',
        name: 'Евгений'
    },
    {
        reply: 'Брала заём несколько раз.Нравится,что при возврате в срок открываются дополнительные тарифы.При необходимости денег очень удобно и проценты маленькие.Всем советую!',
        name: 'Ксения'
    },
    {
        reply: 'Брал 3000 рублей на неделю,вернул во время,открылся новый тариф.Вроде всё в порядке,без обмана.Хороший сервис,если не у кого занять денег до зарплаты.',
        name: 'Олег'
    },
    {
        reply: 'Пользуюсь постоянно,уже открыл максимальный тариф.Жаль,что мало терминалов по городу.',
        name: 'Иван'
    },
    {
        reply: 'Деньги получил.Пришлось переоформлять заём,но в целом всё ок',
        name: 'Виталий'
    },
    {
        reply: 'Пользовался вашими услугами,не отдал во время,набежала небольшая сумма,радует,что процент небольшой.Спасибо!',
        name: 'Денис'
    }
];
