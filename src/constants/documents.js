export default () => [
    {
        text: 'Базовый стандарт защиты прав и интересов физ. и юр. лиц – получателей финансовых услуг',
        doc: 'Bazovy_standart_zaschity_prav_i_interesov_fiz_i_yur_lits__poluchateley_finansovykh_uslug.pdf'
    },
    {
        text: 'Базовый стандарт МФО совершения операций - с 01.07.2018',
        doc: 'Bazovy_standart_MFO_sovershenia_operatsiy_-_s_01_07_2018.pdf'
    },
    {
        text: 'Базовый стандарт по управлению рисками МФО - с 31.07.2017 + 180 дней',
        doc: 'Bazovy_standart_po_upravleniyu_riskami_MFO_-_s_31_07_2017__180_dney.pdf'
    },
    {
        text: 'Приказ 5з от 03.12.19 и Правила предоставления потребительских займов',
        doc: 'Prikaz_5z_ot_03_12_19_i_Pravila_predostavlenia_potrebitelskikh_zaymov.pdf'
    },
    {
        text: 'Свидетельство_ЕГРЮЛ ИНН',
        doc: 'Svidetelstvo_EGRYuL_INN.pdf'
    },
    {
        text: 'Свидетельство_МФО реестр ЦБ',
        doc: 'Svidetelstvo_MFO_reestr_TsB.pdf'
    },
    {
        text: 'Свидетельсто СРО',
        doc: 'Svidetelsto_SRO.pdf'
    },
    {
        text: 'Закон РФ от 07.02.1992 N 2300-1 (ред. от 18.07.2019) О защите прав потребителей',
        doc: 'Zakon_RF_ot_07_02_1992_N_2300-1_red_ot_18_07_2019_O_zaschite_prav_potrebiteley.pdf'
    },
    {
        text: 'Информация об условиях предоставления, использования и возврата потребительского займа ред. 1 от 30.07.2019',
        doc: 'Informatsia_ob_usloviakh_predostavlenia_ispolzovania_i_vozvrata_potrebitelskogo_zayma_red_1_ot_30_07_2019.pdf'
    },
    {
        text: 'Политика в области защиты ПДн ред.1 от 30.07.2019',
        doc: 'Politika_v_oblasti_zaschity_PDn_red_1_ot_30_07_2019.pdf'
    },
    {
        text: 'Согласия и обязательства ред. 1 от 30.07.2019',
        doc: 'Soglasia_i_obyazatelstva_red_1_ot_30_07_2019.pdf'
    },
    {
        text: 'Информация о рисках связанные с заключение и ненадлежащим исполнением условий договора',
        doc: 'Informatsia_o_riskakh_svyazannye_s_zaklyuchenie_i_nenadlezhaschim_ispolneniem_usloviy_dogovora.pdf'
    },
    {
        text: 'Общие условия договора потребительского займа ред. 1 от 30.07.2019',
        doc: 'Obschie_uslovia_dogovora_potrebitelskogo_zayma_red_1_ot_30_07_2019.pdf'
    },
    {
        text: 'Сведения для получателя финансовых услуг (по БС) 1 от 30.07.2019',
        doc: 'Svedenia_dlya_poluchatelya_finansovykh_uslug_po_BS_red_1_ot_30_07_2019.pdf'
    },
    {
        text: 'Соглашение о пролонгации ред. 1 от 30.07.2019',
        doc: 'Soglashenie_o_prolongatsii_red_1_ot_30_07_2019.pdf'
    },
    {
        text: 'Соглашение об аналоге собственноручной подписи (АСП) ред. 1 от 30.07.2019',
        doc: 'Soglashenie_ob_analoge_sobstvennoruchnoy_podpisi_ASP_red_1_ot_30_07_2019.pdf'
    },
    {
        text: 'Список лиц, оказывающих существенное влияние МКК Паскаль вер. 1 от 30.07.2019',
        doc: 'Spisok_lits_okazyvayuschikh_suschestvennoe_vlianie_MKK_Paskal_ver_1_ot_30_07_2019.pdf'
    },
    {
        text: 'Рекомендации по защите информации',
        doc: 'Rekomendatsii_po_zaschite_informatsii.pdf'
    }
];
