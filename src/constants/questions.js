export default {
    getLoan: {
        sectionTitle: 'Получение займа',
        questions: [
            {
                question: 'faq.get.1.q',
                answer: 'faq.get.1.a'
            },
            {
                question: 'faq.get.2.q',
                answer: 'faq.get.2.a'
            },
            {
                question: 'faq.get.3.q',
                answer: 'faq.get.3.a'
            },
            {
                question: 'faq.get.4.q',
                answer: 'faq.get.4.a'
            },
            {
                question: 'faq.get.5.q',
                answer: 'faq.get.5.a'
            },
            {
                question: 'faq.get.6.q',
                answer: 'faq.get.6.a'
            },
            {
                question: 'faq.get.7.q',
                answer: 'faq.get.7.a'
            }
        ]
    },
    payLoan: {
        sectionTitle: 'Погашение',
        questions: [
            {
                question: 'faq.pay.1.q',
                answer: 'faq.pay.1.a'
            },
            {
                question: 'faq.pay.2.q',
                answer: 'faq.pay.2.a'
            },
            {
                question: 'faq.pay.3.q',
                answer: 'faq.pay.3.a'
            },
            {
                question: 'faq.pay.4.q',
                answer: 'faq.pay.4.a'
            },
            {
                question: 'faq.pay.5.q',
                answer: 'faq.pay.5.a'
            }
        ]
    },
    security: {
        sectionTitle: 'Аккаунт',
        questions: [
            {
                question: 'faq.acc.1.q',
                answer: 'faq.acc.1.a'
            }
        ]
    }
};