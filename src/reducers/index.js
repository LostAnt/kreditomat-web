import { combineReducers } from 'redux';

import user from './user';
import auth from './auth';
import activeLoan from './active-loan';
import loansHistory from './loans-history';
import creditCards from './credit-cards';

const reducer = combineReducers({
    user,
    auth,
    activeLoan,
    loansHistory,
    creditCards
});

export default reducer;
