
export const AUTH = 'AUTH';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAILURE = 'AUTH_FAILURE';
export const UPDATE_TOKENS = 'UPDATE_TOKENS';
export const REFRESH_TOKEN = 'REFRESH_TOKEN';
export const REFRESH_TOKEN_SUCCESS = 'REFRESH_TOKEN_SUCCESS';
export const REFRESH_TOKEN_FAILURE = 'REFRESH_TOKEN_FAILURE';
export const LOGOUT = 'LOGOUT';

const initialState = {
    isLoading: false,
    authToken: null,
    refreshToken: null
};

export default (state = initialState, action) => {
    switch (action.type) {
    case AUTH: {
        return {
            ...state,
            isLoading: true
        };
    }

    case AUTH_SUCCESS: {
        return {
            ...state,
            authToken: action.payload.token,
            refreshToken: action.payload.refreshToken,
            isLoading: false
        };
    }

    case AUTH_FAILURE: {
        return {
            ...state,
            isLoading: false
        };
    }

    case UPDATE_TOKENS: {
        return {
            ...state,
            authToken: action.payload.authToken,
            refreshToken: action.payload.refreshToken
        };
    }

    case REFRESH_TOKEN: {
        return {
            ...state,
            isLoading: true
        };
    }

    case REFRESH_TOKEN_SUCCESS: {
        return {
            ...state,
            authToken: action.payload.token,
            refreshToken: action.payload.refreshToken,
            isLoading: false
        };
    }

    case REFRESH_TOKEN_FAILURE: {
        return {
            ...state,
            isLoading: false
        };
    }

    case LOGOUT: {
        return {
            ...state,
            authToken: null,
            refreshToken: null
        };
    }

    default:
        return state;
    }
};
