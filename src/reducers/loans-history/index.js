import { LOGOUT } from 'reducers/auth';

export const FETCH_LOANS_HISTORY = 'FETCH_LOANS_HISTORY';
export const FETCH_LOANS_HISTORY_SUCCESS = 'FETCH_LOANS_HISTORY_SUCCESS';
export const FETCH_LOANS_HISTORY_FAILURE = 'FETCH_LOANS_HISTORY_FAILURE';
export const RESET_LOANS_HISTORY = 'RESET_LOANS_HISTORY';

const initialState = {
    isLoading: false,
    isLoaded: false,
    loans: null
};

export default (state = initialState, action) => {
    switch (action.type) {
    case FETCH_LOANS_HISTORY: {
        return {
            ...state,
            isLoading: true,
            isLoaded: false
        };
    }

    case FETCH_LOANS_HISTORY_SUCCESS: {
        return {
            ...state,
            loans: action.payload.credits,
            isLoading: false,
            isLoaded: true
        };
    }

    case FETCH_LOANS_HISTORY_FAILURE: {
        return {
            ...state,
            isLoading: false,
            isLoaded: false
        };
    }

    case LOGOUT:
    case RESET_LOANS_HISTORY: {
        return initialState;
    }

    default:
        return state;
    }
};
