import { LOGOUT } from 'reducers/auth';

export const FETCH_CREDIT_CARDS = 'FETCH_CREDIT_CARDS';
export const FETCH_CREDIT_CARDS_SUCCESS = 'FETCH_CREDIT_CARDS_SUCCESS';
export const FETCH_CREDIT_CARDS_FAILURE = 'FETCH_CREDIT_CARDS_FAILURE';

const initialState = {
    isLoading: false,
    isLoaded: false,
    list: null
};

export default (state = initialState, action) => {
    switch (action.type) {
    case FETCH_CREDIT_CARDS: {
        return {
            ...state,
            isLoading: true,
            isLoaded: false
        };
    }

    case FETCH_CREDIT_CARDS_SUCCESS: {
        return {
            ...state,
            list: action.payload.creditCards,
            isLoading: false,
            isLoaded: true
        };
    }

    case FETCH_CREDIT_CARDS_FAILURE: {
        return {
            ...state,
            isLoading: false,
            isLoaded: false
        };
    }

    case LOGOUT: {
        return initialState;
    }

    default:
        return state;
    }
};
