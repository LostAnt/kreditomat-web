import { LOGOUT } from 'reducers/auth';

export const FETCH_ACTIVE_LOAN = 'FETCH_ACTIVE_LOAN';
export const FETCH_ACTIVE_LOAN_SUCCESS = 'FETCH_ACTIVE_LOAN_SUCCESS';
export const FETCH_ACTIVE_LOAN_FAILURE = 'FETCH_ACTIVE_LOAN_FAILURE';
export const RESET_ACTIVE_LOAN = 'RESET_ACTIVE_LOAN';

const initialState = {
    isLoading: false,
    isLoaded: false,
    loan: null
};

export default (state = initialState, action) => {
    switch (action.type) {
    case FETCH_ACTIVE_LOAN: {
        return {
            ...state,
            isLoading: true,
            isLoaded: false
        };
    }

    case FETCH_ACTIVE_LOAN_SUCCESS: {
        return {
            ...state,
            loan: action.payload.credit,
            isLoading: false,
            isLoaded: true
        };
    }

    case FETCH_ACTIVE_LOAN_FAILURE: {
        return {
            ...state,
            isLoading: false,
            isLoaded: false
        };
    }

    case LOGOUT:
    case RESET_ACTIVE_LOAN: {
        return initialState;
    }

    default:
        return state;
    }
};
