import { LOGOUT } from 'reducers/auth';

export const FETCH_USER = 'FETCH_USER';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';
export const RESET_USER = 'RESET_USER';

const initialState = {
    isLoading: false,
    isLoaded: false,
    profile: {},
    authToken: null,
    refreshToken: null
};

export default (state = initialState, action) => {
    switch (action.type) {
    case FETCH_USER: {
        return {
            ...state,
            isLoading: true,
            isLoaded: false
        };
    }

    case FETCH_USER_SUCCESS: {
        return {
            ...state,
            profile: action.payload.profile,
            isLoading: false,
            isLoaded: true
        };
    }

    case FETCH_USER_FAILURE: {
        return {
            ...state,
            isLoading: false,
            isLoaded: false
        };
    }

    case LOGOUT:
    case RESET_USER: {
        return initialState;
    }

    default:
        return state;
    }
};
