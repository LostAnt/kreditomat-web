import { Provider } from 'react-redux';

import store from 'store';

const withReduxProvider = (BaseComponent) => (props) => (
    <Provider store={store}>
        <BaseComponent {...props} />
    </Provider>
);
export default withReduxProvider;
