
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { refreshTokens } from 'actions/auth';

const hoc = (WrappedComponent) => {
    class WithRefreshTokens extends React.Component {
        componentDidMount() {
            if (localStorage.authToken) {
                setInterval(() => {
                    this.props.actions.refreshTokens(localStorage.authToken, localStorage.refreshToken);
                }, 550000);
            }
        }

        render() {
            return (
                <WrappedComponent {...this.props} />
            );
        }
    }
    const mapDispatchToProps = (dispatch) => ({
        actions: bindActionCreators({
            refreshTokens
        }, dispatch)
    });

    return connect(null, mapDispatchToProps)(WithRefreshTokens);
};

export default hoc;
