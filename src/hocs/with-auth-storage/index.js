
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { updateTokens } from 'actions/auth';

const hoc = (WrappedComponent) => {
    class WithAuthStorage extends React.Component {
        componentDidMount() {
            console.log(localStorage.authToken, localStorage.refreshToken);
            if (localStorage.authToken && localStorage.refreshToken) {
                this.props.actions.updateTokens(
                    localStorage.authToken,
                    localStorage.refreshToken
                );
            }
        }

        componentDidUpdate(prevProps) {
            if (prevProps.authToken !== this.props.authToken
                || prevProps.refreshToken !== this.props.refreshToken) {
                localStorage.setItem('authToken', this.props.authToken);
                localStorage.setItem('refreshToken', this.props.refreshToken);
            }
        }

        render() {
            return (
                <WrappedComponent {...this.props} />
            );
        }
    }

    const mapStateToProps = (state) => ({
        authToken: state.auth.authToken,
        refreshToken: state.auth.refreshToken
    });

    const mapDispatchToProps = (dispatch) => ({
        actions: bindActionCreators({
            updateTokens
        }, dispatch)
    });

    return connect(mapStateToProps, mapDispatchToProps)(WithAuthStorage);
};

export default hoc;
