import { api } from 'utils/apiClient';
import Router from 'next/router';

import store from 'store';

import {
    AUTH,
    AUTH_SUCCESS,
    AUTH_FAILURE,
    UPDATE_TOKENS,
    REFRESH_TOKEN,
    REFRESH_TOKEN_SUCCESS,
    REFRESH_TOKEN_FAILURE,
    LOGOUT
} from 'reducers/auth';

export const auth = (login, password) => async (dispatch) => {
    dispatch({
        type: AUTH
    });

    try {
        const response = await api.post('users/login', { login, password });

        dispatch({
            type: AUTH_SUCCESS,
            payload: {
                ...response
            }
        });
    } catch (error) {
        dispatch({
            type: AUTH_FAILURE
        });

        throw error;
    }
};

export const updateTokens = (authToken, refreshToken) => async (dispatch) => {
    dispatch({
        type: UPDATE_TOKENS,
        payload: {
            authToken,
            refreshToken
        }
    });
};

export const refreshTokens = (authToken, refreshToken) => async (dispatch) => {
    dispatch({
        type: REFRESH_TOKEN
    });

    try {
        const response = await api.post('token/refresh', { token: authToken, refreshToken });

        dispatch({
            type: REFRESH_TOKEN_SUCCESS,
            payload: {
                ...response
            }
        });

        return;
    } catch (error) {
        dispatch({
            type: REFRESH_TOKEN_FAILURE
        });

        dispatch({
            type: LOGOUT
        });

        throw error;
    }
};

export const logout = () => async (dispatch) => {
    dispatch({
        type: LOGOUT
    });

    Router.push('/auth');
};
