import { api } from 'utils/apiClient';

import {
    FETCH_USER,
    FETCH_USER_SUCCESS,
    FETCH_USER_FAILURE,
    RESET_USER
} from 'reducers/user';

export const fetchUser = () => async (dispatch) => {
    dispatch({
        type: FETCH_USER
    });

    try {
        const response = await api.get('borrower/profile');

        dispatch({
            type: FETCH_USER_SUCCESS,
            payload: response
        });
    } catch (error) {
        console.log(error);
        dispatch({
            type: FETCH_USER_FAILURE
        });
    }
};

export const updateUser = () => async (dispatch) => {
    dispatch({
        type: FETCH_USER
    });
};

export const resetUser = () => async (dispatch) => {
    dispatch({
        type: RESET_USER
    });
};