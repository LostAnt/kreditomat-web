/* eslint-disable import/prefer-default-export */

import { api } from 'utils/apiClient';

import {
    FETCH_CREDIT_CARDS,
    FETCH_CREDIT_CARDS_SUCCESS,
    FETCH_CREDIT_CARDS_FAILURE
} from 'reducers/credit-cards';

export const fetchCreditCards = () => async (dispatch) => {
    dispatch({
        type: FETCH_CREDIT_CARDS
    });

    try {
        const response = await api.get('borrower/creditcards');

        dispatch({
            type: FETCH_CREDIT_CARDS_SUCCESS,
            payload: response
        });
    } catch (error) {
        dispatch({
            type: FETCH_CREDIT_CARDS_FAILURE
        });
    }
};
