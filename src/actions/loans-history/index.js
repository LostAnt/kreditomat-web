/* eslint-disable import/prefer-default-export */

import { api } from 'utils/apiClient';

import {
    FETCH_LOANS_HISTORY,
    FETCH_LOANS_HISTORY_SUCCESS,
    FETCH_LOANS_HISTORY_FAILURE,
    RESET_LOANS_HISTORY
} from 'reducers/loans-history';

export const fetchLoansHistory = () => async (dispatch) => {
    dispatch({
        type: FETCH_LOANS_HISTORY
    });

    try {
        const response = await api.get('credits/');

        dispatch({
            type: FETCH_LOANS_HISTORY_SUCCESS,
            payload: response
        });
    } catch (error) {
        console.log(error);
        dispatch({
            type: FETCH_LOANS_HISTORY_FAILURE
        });
    }
};

export const resetLoansHistory = () => async (dispatch) => {
    dispatch({
        type: RESET_LOANS_HISTORY
    });
};
