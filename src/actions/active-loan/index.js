/* eslint-disable import/prefer-default-export */

import { api } from 'utils/apiClient';

import {
    FETCH_ACTIVE_LOAN,
    FETCH_ACTIVE_LOAN_SUCCESS,
    FETCH_ACTIVE_LOAN_FAILURE,
    RESET_ACTIVE_LOAN
} from 'reducers/active-loan';

export const fetchActiveLoan = () => async (dispatch) => {
    dispatch({
        type: FETCH_ACTIVE_LOAN
    });

    try {
        const response = await api.get('credits/active');

        dispatch({
            type: FETCH_ACTIVE_LOAN_SUCCESS,
            payload: response
        });
    } catch (error) {
        dispatch({
            type: FETCH_ACTIVE_LOAN_FAILURE
        });
    }
};

export const resetActiveLoan = () => async (dispatch) => {
    dispatch({
        type: RESET_ACTIVE_LOAN
    });
};
